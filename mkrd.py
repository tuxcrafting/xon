import os
import sys
import struct

stdout = open(1, "wb")

for x in sys.argv[1:]:
    p = os.path.basename(x).encode("utf8")
    stdout.write(struct.pack("<I", len(p)))
    stdout.write(p)
    with open(x, "rb") as f:
        data = f.read()
    stdout.write(struct.pack("<I", len(data)))
    stdout.write(data)
stdout.write(struct.pack("<I", 0))
