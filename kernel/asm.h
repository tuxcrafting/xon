#pragma once

#define PIC1 0x20
#define PIC2 0xA0
#define PIC1_CMD PIC1
#define PIC2_CMD PIC2
#define PIC1_DAT (PIC1+1)
#define PIC2_DAT (PIC2+1)

#define PIC_EOI 0x20

#define ICW1_ICW4 0x01
#define ICW1_SINGLE 0x02
#define ICW1_INTERVAL4 0x04
#define ICW1_LEVEL 0x08
#define ICW1_INIT 0x10

#define ICW4_8086 0x01
#define ICW4_AUTO 0x02
#define ICW4_BUF_SLAVE 0x04
#define ICW4_BUF_MASTER 0x08
#define ICW4_SFNM 0x10

#define PIT 0x40
#define PIT_DAT0 PIT
#define PIT_DAT1 (PIT+1)
#define PIT_DAT2 (PIT+2)
#define PIT_CMD (PIT+3)

#define examine1(a) asm("mov %0, %%rax; int $0x50" :: "a"((U64)(a)))
#define outb(port, data) \
    asm volatile("outb %0, %1" :: "a"((U8)(data)), "Nd"((U16)port))
#define inb(port, data) \
    asm volatile("inb %1, %0" : "=a"(data) : "Nd"((U16)port))
#define io_wait asm volatile("outb %%al, $0x80" :: "a"(0))

#define cli asm volatile("cli")
#define sti asm volatile("sti")
#define iret asm volatile("iretq")

#define _savereg(i, r) "movq %r" #r ", (registers_state+" #i "*8);"
#define _rstrreg(i, r) "movq (registers_state+" #i "*8), %r" #r ";"
#define _regapply(f) f(0,ax) f(1,bx) f(2,cx) f(3,dx) f(4,si) f(5,di) f(6,bp) \
                     f(7,8) f(8,9) f(9,10) f(11,12) f(12,13) f(13,14) f(14,15)
#define pusha asm volatile(_regapply(_savereg) \
                           "mov %rsp, (registers_state+17*8)")
#define popa asm volatile(_regapply(_rstrreg))
#define save_errcode \
    asm volatile("pop %rax; movq %rax, (registers_state+15*8)")
#define save_rip \
    asm volatile("movq (%rsp), %rax; movq %rax, (registers_state+16*8)")

inline void pic_sendEOI(U64 irq) {
    if (irq >= 8) {
        outb(PIC2_CMD, PIC_EOI);
    }
    outb(PIC1_CMD, PIC_EOI);
}
