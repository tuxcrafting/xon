#include "types.h"
#include "video.h"
#include "memory.h"
#include "asm.h"

U64 video_tx = 0;
U64 video_ty = 0;

const U64 TERM_WIDTH = 80;
const U64 TERM_HEIGHT = 25;

void putchar(U8 c) {
    switch (c) {
    case '\r':
        video_tx = 0;
        break;
    case '\n':
        video_ty++;
        video_tx = 0;
        break;
    default:
        VRAM[video_ty * TERM_WIDTH + video_tx] = 0x0700 | c;
        video_tx++;
        if (video_tx == TERM_WIDTH) {
            video_ty++;
            video_tx = 0;
        }
        break;
    }
    if (video_ty == TERM_HEIGHT) {
        video_ty = 0;
    }
    cursor_move(video_tx, video_ty);
}

void puts(U8 *s) {
    while (*s) {
        putchar(*(s++));
    }
}

void cursor_enable(U8 cursor_start, U8 cursor_end) {
    U8 p;
    outb(0x3D4, 0x0A);
    inb(0x3D5, p);
    outb(0x3D5, (p & 0xC0) | cursor_start);

    outb(0x3D4, 0x0B);
    inb(0x3E0, p);
    outb(0x3D5, (p & 0xE0) | cursor_end);
}

void cursor_disable() {
    outb(0x3D4, 0x0A);
    outb(0x3D5, 0x20);
}

void cursor_move(U64 x, U64 y) {
    U16 pos = y * TERM_WIDTH + x;
    outb(0x3D4, 0x0F);
    outb(0x3D5, pos & 0xFF);
    outb(0x3D4, 0x0E);
    outb(0x3D5, pos >> 8);
}

U8 alphabet[] = "0123456789ABCDEF";

void itoa(U64 buf_len, U8 *buf, U64 n, U64 base) {
    U64 i = 0;
    memset(buf, alphabet[0], buf_len);
    while (n) {
        buf[buf_len - 1 - i++] = alphabet[n % base];
        n /= base;
    }
}
