#include "types.h"
#include "asm.h"
#include "multiboot.h"
#include "video.h"
#include "memory.h"
#include "vm.h"
#include "vm_opcodes.h"

#define PIT_TICK_MS 10
#define PIT_BASE_FREQ 1193182
#define PIT_DIVIDER 1193

volatile U64 pit_ticks;
volatile U64 milliseconds;
volatile U64 context_switch;

U64 registers_state[18];

void print_regstate() {
    U8 buf[17];
    buf[16] = 0;
#define PRINT_REG(s, i, c) \
    itoa(16, buf, registers_state[i], 16); \
    puts(s " "); \
    puts(buf); \
    putchar(c)
    PRINT_REG("RAX", 0, ' '); PRINT_REG("RBX", 1, ' ');
    PRINT_REG("RCX", 2, '\n'); PRINT_REG("RDX", 3, ' ');
    PRINT_REG("RSI", 4, ' '); PRINT_REG("RDI", 5, '\n');
    PRINT_REG("RBP", 6, ' '); PRINT_REG("R8 ", 7, ' ');
    PRINT_REG("R9 ", 8, '\n'); PRINT_REG("R10", 9, ' ');
    PRINT_REG("R11", 10, ' '); PRINT_REG("R12", 11, '\n');
    PRINT_REG("R13", 12, ' '); PRINT_REG("R14", 13, ' ');
    PRINT_REG("R15", 14, '\n'); PRINT_REG("RSP", 17, ' ');
    PRINT_REG("RIP", 16, '\n');
}

#define int_handler(f) __attribute__((naked)) void n_handler_##f() { \
    pusha; \
    asm volatile("call handler_" #f); \
    popa; \
    iret; \
}
#define int_handler_h(f, h) __attribute__((naked)) void n_handler_##f() { \
    pusha; \
    h \
    asm volatile("call handler_" #f); \
    popa; \
    iret; \
}

__attribute__((naked)) void n_handler_nop() {
    iret;
}
void handler_GPF() {
    pusha;
    puts("#GPF\n");
    print_regstate();
    for (;;) asm("hlt");
}
int_handler_h(GPF, save_errcode; save_rip;)
void handler_PF() {
    U64 err = registers_state[15];
    puts("#PF on ");
    if (err & (1 << 1)) {
        puts("write");
    } else {
        puts("read");
    }
    if (!(err & (1 << 0))) {
        puts(" (nonexistent page)");
    }
    putchar('\n');
    U8 buf[17];
    buf[16] = 0;
    U64 cr2;
    asm("mov %%cr2, %0" : "=a"(cr2));
    itoa(16, buf, cr2, 16);
    puts("Address:");
    puts(buf);
    putchar('\n');
    print_regstate();
    for (;;) asm("hlt");
}
int_handler_h(PF, save_errcode; save_rip;)
void handler_irq0() {
    pit_ticks++;
    milliseconds += PIT_TICK_MS;
    context_switch = 1;
    pic_sendEOI(0);
}
int_handler(irq0)
void handler_irq1() {
    pic_sendEOI(0);
}
int_handler(irq1)
void handler_debug() {
    puts("Debug\n");
    print_regstate();
    for (;;) asm("hlt");
}
int_handler_h(debug, save_rip;)
void handler_vmfault() {
    if (vm->catch_stack != NULL) {
        vm->pc = stack_pop(&vm->return_stack);
        U64 c = xvm_stack_pop(&vm->catch_stack);
        U64 data_depth = stack_pop(&vm->catch_stack);
        U64 return_depth = stack_pop(&vm->catch_stack);
        U64 i = 0;
        return_depth = return_depth - stack_depth(vm->return_stack);
        data_depth = data_depth - stack_depth(vm->data_stack);
        while (vm->return_stack != NULL && i < return_depth) {
            stack_pop(&vm->return_stack);
            i++;
        }
        i = 0;
        while (vm->data_stack != NULL && i < data_depth) {
            xvm_stack_pop(&vm->data_stack);
            i++;
        }
        vm_int_t *err = gc_calloc(sizeof(vm_int_t), T_INT);
        err->n = registers_state[0];
        xvm_stack_push(&vm->data_stack, make_ptr(err, T_INT));
        xvm_call((vm_function_t*)get_ptr_value(c));
    } else {
        xvm_destroy_process(cur_pid);
    }
}
__attribute__((naked)) void n_handler_vmfault() {
    asm volatile("call handler_vmfault;"
                 "movq $_xvm_step_end, (%rsp);"
                 "movq (xvm_step_rsp), %rax;"
                 "movq %rax, 24(%rsp)");
    iret;
}

void kerror(U8 *s) {
    puts("Kernel error!\n");
    puts(s);
    for (;;) { asm("hlt"); }
}

typedef struct gate_struct64 {
    U16 offset_low;
    U16 segment;
    U32 ist : 3, zero0 : 5, type : 5, dpl : 2, p : 1;
    U16 offset_middle;
    U32 offset_high;
    U32 zero1;
} __attribute__((packed)) gate_struct64;
gate_struct64 *idt_entries;
struct {
    U16 limit;
    U64 base;
} __attribute__((packed)) idtr;

U64 pg_address;
U64 ****pg_pml4;
void do_page(U64 addr, U64 vaddr) {
    U64 pml4_i = (vaddr >> 39) & 511;
    U64 pdp_i = (vaddr >> 30) & 511;
    U64 pd_i = (vaddr >> 21) & 511;
    U64 p_i = (vaddr >> 12) & 511;
    // for PML4, PDP and PD, check if the corresponding entry exists
    // if it doesn't, create it
    if (((U64)pg_pml4[pml4_i] & 1) == 0) {
        pg_pml4[pml4_i] = (U64***)(pg_address | 3);
        memset((U8*)pg_address, 0, 4096);
        pg_address += 4096;
    }
    U64 ***pml4_e = (U64***)((U64)pg_pml4[pml4_i] & ~4095);
    if (((U64)pml4_e[pdp_i] & 1) == 0) {
        pml4_e[pdp_i] = (U64**)(pg_address | 3);
        memset((U8*)pg_address, 0, 4096);
        pg_address += 4096;
    }
    U64 **pdp_e = (U64**)((U64)pml4_e[pdp_i] & ~4095);
    if (((U64)pdp_e[pd_i] & 1) == 0) {
        pdp_e[pd_i] = (U64*)(pg_address | 3);
        memset((U8*)pg_address, 0, 4096);
        pg_address += 4096;
    }
    U64 *pd_e = (U64*)((U64)pdp_e[pd_i] & ~4095);
    pd_e[p_i] = addr | 3;
}

multiboot_info_t *mbi;

U64 kstage1(U64 _mbi) {
    mbi = (multiboot_info_t*)_mbi;

    // init VGA
    cursor_enable(0, 13);

    // check for memory map
    if ((mbi->flags & (1 << 6)) == 0) {
        kerror("Can't get memory map from bootloader");
    }
    // page memory
    pg_address = M(16);
    pg_pml4 = (U64****)pg_address;
    memset((U8*)pg_pml4, 0, 4096);
    // id map first 16MB
    pg_address += 4096;
    for (U64 i = 4096; i < M(16); i += 4096) {
        do_page(i, i);
    }
    // map the rest while being aware of holes
    U64 vaddr = M(16);
    multiboot_memory_map_t *mmap =
        (multiboot_memory_map_t*)(U64)mbi->mmap_addr;
    while ((U64)mmap < (U64)mbi->mmap_addr + (U64)mbi->mmap_length) {
        if (mmap->type == 1) {
            U64 base = mmap->addr;
            U64 limit = base + mmap->len;
            if (base < M(16)) {
                base = M(16);
            }
            // start at upper page boundary
            if ((base & 4095) != 0) {
                base += 4096;
            }
            base &= ~4095;
            for (U64 i = base; i < limit; i += 4096) {
                do_page(i, vaddr);
                vaddr += 4096;
            }
        }
        mmap = (multiboot_memory_map_t*)(
            (U64)mmap + mmap->size + sizeof(mmap->size));
    }
    asm("mov %0, %%rax;"
        "and $~4095, %%rax;"
        "mov %%rax, %%cr3" :: "r"((U64)pg_pml4 & ~4095));

    // enable SSE
    asm("mov %cr0, %rax;"
        "and $0xFFFB, %ax;"
        "or $0x2, %ax;"
        "mov %rax, %cr0;"
        "mov %cr4, %rax;"
        "or $3 << 9, %ax;"
        "mov %rax, %cr4");

    // define memory allocation bitmap
    heap_size = vaddr;
    mem_free = heap_size;
    alloc_bitmap = (U8*)pg_address;
    last_allocated = 0;
    memset(alloc_bitmap, 0, heap_size / MALLOC_BLOCK_SIZE / 4);
    malloc(pg_address + heap_size / MALLOC_BLOCK_SIZE / 4);

    // allocate stack
    U64 stack = (U64)malloc(M(1)) + M(1);
    if (stack == NULL) {
        kerror("Can't allocate stack");
    }

#define put_idt_addr(ent, addr) \
    ent.offset_low = (U64)(addr) & 0xFFFF; \
    ent.offset_middle = ((U64)(addr) >> 16) & 0xFFFF; \
    ent.offset_high = (U64)(addr) >> 32
    // setup interrupts
    idt_entries = calloc(sizeof(gate_struct64) * 256);
    for (U64 i = 0; i < 256; i++) {
        idt_entries[i].segment = 0x8;
        idt_entries[i].ist = 0;
        idt_entries[i].type = 14;
        idt_entries[i].dpl = 0;
        idt_entries[i].p = 1;
        put_idt_addr(idt_entries[i], &n_handler_nop);
    }
    // set fault handlers
    put_idt_addr(idt_entries[0x0D], &n_handler_GPF);
    put_idt_addr(idt_entries[0x0E], &n_handler_PF);
    // set IRQ handlers
    put_idt_addr(idt_entries[0x20], &n_handler_irq0);
    put_idt_addr(idt_entries[0x21], &n_handler_irq1);
    // set debug handler
    put_idt_addr(idt_entries[0x50], &n_handler_debug);
    // set VM fault handler
    put_idt_addr(idt_entries[0x60], &n_handler_vmfault);
    // load idt
    idtr.limit = sizeof(gate_struct64) * 256 - 1;
    idtr.base = (U64)idt_entries;
    asm("lidt (idtr)");

    // setup PIC
    // remap to 0x20:0x28
    outb(PIC1_CMD, ICW1_INIT | ICW1_ICW4);
    io_wait;
    outb(PIC2_CMD, ICW1_INIT | ICW1_ICW4);
    io_wait;
    outb(PIC1_DAT, 0x20);
    io_wait;
    outb(PIC2_DAT, 0x28);
    io_wait;
    outb(PIC1_DAT, 4);
    io_wait;
    outb(PIC2_DAT, 2);
    io_wait;
    outb(PIC1_DAT, ICW4_8086);
    io_wait;
    outb(PIC2_DAT, ICW4_8086);
    io_wait;
    // mask out all IRQs except IRQ0 (PIT) and IRQ1 (keyboard)
    outb(PIC1_DAT, 0b11111100);
    outb(PIC2_DAT, 0b11111111);

    outb(PIT_DAT0, PIT_DIVIDER & 0xFF);
    outb(PIT_DAT0, PIT_DIVIDER >> 8);
    sti;

    return stack;
}

void kstage2() {
    // scan initrd
    U8 *init_xvm = NULL;
    U8 *mod_base = NULL;
    U64 mod_length = 0;
    for (U64 i = 0; i < mbi->mods_count; i++) {
        struct multiboot_mod_list *l =
            (struct multiboot_mod_list*)(mbi->mods_addr
                    + i * sizeof(struct multiboot_mod_list));
        void *ptr = (void*)(U64)l->mod_start;
        while (ptr <= (void*)(U64)l->mod_end) {
            U32 name_len = *(U32*)ptr;
            if (name_len == 0) {
                break;
            }
            ptr += 4;
            U64 name_hash = hash_bytes(name_len, ptr);
            ptr += name_len;
            U32 data_len = *(U32*)ptr;
            ptr += 4;
            if (name_hash == 0x7AD47A12D9EE783E) { // init.xvm
                init_xvm = ptr;
                mod_base = (U8*)(U64)l->mod_start;
                mod_length = l->mod_end - l->mod_start;
                break;
            }
            ptr += data_len;
        }
        if (init_xvm != NULL) {
            break;
        }
    }
    if (init_xvm == NULL) {
        kerror("Can't find init.xvm in initrd");
    }

    // create bytestring for the initrd
    vm_bytestring_t *initrd_bs =
        gc_calloc(sizeof(vm_bytestring_t), T_BYTESTRING);
    initrd_bs->len = mod_length;
    initrd_bs->data = mod_base;

    // start vm
    cur_pid = xvm_create_process();
    vm = vm_list[cur_pid];
    xvm_create(init_xvm);
    vm->parent = 0;
    vm->permissions = PERM_NATIVE_CTL | PERM_PERM_CTL;
    xvm_stack_push(&vm->data_stack, make_ptr(initrd_bs, T_BYTESTRING));

    while (max_pid > 0) {
        if (context_switch
         || vm_list[cur_pid] == NULL
         || vm_list[cur_pid]->flags & VF_IPC_WAIT) {
            xvm_schedule();
            context_switch = 0;
        }
        if (vm_list[cur_pid] != NULL) {
            xvm_step();
        }
    }

    for (;;) asm("hlt");
}
