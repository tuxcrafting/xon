#pragma once

#define MAX_PIDS 1024

extern struct vm_t *vm;
extern struct vm_t *vm_list[];
extern U64 max_pid, cur_pid;
extern U64 xvm_step_rsp;
extern struct stack_t *gc_root;

typedef enum ptr_tag_t {
    T_INVALID = 0,
    T_INT = 1,
    T_FLOAT = 2,
    T_CONS = 3,
    T_TUPLE = 4,
    T_BYTESTRING = 5,
    T_FUNCTION = 6,
    T_PLACEHOLDER = 7,
    T_HASHMAP = 8,
} ptr_tag_t;

inline ptr_tag_t get_ptr_tag(U64 ptr) {
    return ptr >> 56;
}
inline void *get_ptr_value(U64 ptr) {
    return (void*)(ptr & 0xFFFFFFFFFFFFFF);
}
inline U64 make_ptr(void *ptr, ptr_tag_t tag) {
    return (U64)ptr | ((U64)tag << 56);
}

#define TF_NOGC_DATA (1<<0)

typedef struct vm_int_t {
    S32 refcnt;
    U32 flags;
    S64 n;
} vm_int_t;
typedef struct vm_float_t {
    S32 refcnt;
    U32 flags;
    F64 n;
} vm_float_t;
typedef struct vm_cons_t {
    S32 refcnt;
    U32 flags;
    U64 car;
    U64 cdr;
} vm_cons_t;
typedef struct vm_tuple_t {
    S32 refcnt;
    U32 flags;
    U64 len;
    U64 *items;
    U64 parent;
} vm_tuple_t;
typedef struct vm_bytestring_t {
    S32 refcnt;
    U32 flags;
    U64 len;
    U8 *data;
    U64 parent;
} vm_bytestring_t;
typedef struct vm_function_t {
    S32 refcnt;
    U32 flags;
    U64 pc;
    U64 len;
    U64 *scopes;
} vm_function_t;
typedef struct vm_hashmap_t {
    S32 refcnt;
    U32 flags;
    struct hashtree_t *tree;
} vm_hashmap_t;

typedef struct stack_t {
    U64 data;
    struct stack_t *previous;
} stack_t;

typedef struct chunk_t {
    U32 name;
    U32 len;
    U8 *data;
} chunk_t;

typedef struct hashtree_t {
    union {
        struct {
            struct hashtree_t *branch[16];
        };
        struct {
            stack_t *keys;
            stack_t *values;
        };
    };
} hashtree_t;

#define VF_IPC_WAIT (1 << 0)

typedef struct vm_t {
    U16 flags;
    U32 permissions;
    S64 parent;
    U64 pc;
    U64 bytecode_len;
    U8 *bytecode;
    U64 globals_len;
    U64 *globals;
    stack_t *data_stack;
    stack_t *return_stack;
    stack_t *locals_stack;
    stack_t *catch_stack;
    stack_t *ipc_data;
    stack_t *ipc_pid;
} vm_t;

U64 hash_bytes(U64 len, U8 *b);
U64 hash_obj(U64 obj);
U64 is_equal(U64 a, U64 b);
stack_t *stack_push(stack_t **stack, U64 data);
void xvm_stack_push(stack_t **stack, U64 data);
U64 stack_pop(stack_t **stack);
U64 xvm_stack_pop(stack_t **stack);
U64 stack_depth(stack_t *stack);
void hashmap_set(hashtree_t *tree, U64 key, U64 value);
U64 hashmap_get(hashtree_t *tree, U64 key);
#define xvm_gc() gc_sweep(&gc_root)
void gc_scan_stack(stack_t *stack);
void gc_sweep(stack_t **stack);
void *gc_malloc(U64 size, U8 tag);
void *gc_calloc(U64 size, U8 tag);
U64 xvm_create_process();
void xvm_destroy_process(U64 pid);
void xvm_schedule();
void xvm_next_chunk(chunk_t *chunk, U8 **data);
U64 xvm_create(U8 *exe);
void xvm_call(vm_function_t *f);
void xvm_step();
