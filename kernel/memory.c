#include "types.h"
#include "memory.h"
#include "video.h"
#include "asm.h"

U64 heap_size;
U64 mem_free;
U8 *alloc_bitmap;
U64 last_allocated;

void *malloc(U64 length) {
    if (length % MALLOC_BLOCK_SIZE != 0) {
        length /= MALLOC_BLOCK_SIZE;
        length += 1;
    } else {
        length /= MALLOC_BLOCK_SIZE;
    }
    if (length == 0) {
        return NULL;
    }
    U64 base = last_allocated;
    U64 run = 0;
    for (U64 i = last_allocated, j = 0;
         j < heap_size / MALLOC_BLOCK_SIZE;
         i++, j++) {
        if (i == heap_size / MALLOC_BLOCK_SIZE) {
            i = 0;
        }
        U8 n = alloc_bitmap[i / 4];
        if (((n >> (i % 4 * 2)) & 1) == 0) {
            run += 1;
        } else {
            base = i + 1;
            run = 0;
        }
        if (run == length) {
            break;
        }
    }
    if (run == length) {
        for (U64 i = base; i < base + length; i++) {
            alloc_bitmap[i / 4] |= 1 << (i % 4 * 2);
            if (i == base + length - 1) {
                alloc_bitmap[i / 4] &= ~(1 << (i % 4 * 2 + 1));
            } else {
                alloc_bitmap[i / 4] |= 1 << (i % 4 * 2 + 1);
            }
        }
        mem_free -= length * MALLOC_BLOCK_SIZE;
        last_allocated = base + length;
        return (void*)(base * MALLOC_BLOCK_SIZE);
    }
    return NULL;
}

void *calloc(U64 length) {
    U8 *p = malloc(length);
    memset(p, 0, length);
    return (void*)p;
}

void free(void *ptr) {
    if (ptr == NULL) {
        return;
    }
    for (U64 i = (U64)ptr / MALLOC_BLOCK_SIZE;; i++) {
        if ((alloc_bitmap[i / 4] & (1 << (i % 4 * 2))) == 0) {
            return;
        }
        mem_free += MALLOC_BLOCK_SIZE;
        alloc_bitmap[i / 4] &= ~(1 << (i % 4 * 2));
        if ((alloc_bitmap[i / 4] & (1 << (i % 4 * 2 + 1))) == 0) {
            return;
        }
        alloc_bitmap[i / 4] &= ~(1 << (i % 4 * 2 + 1));
    }
    last_allocated = (U64)ptr / MALLOC_BLOCK_SIZE;
}

U64 is_allocated(void *ptr) {
    U64 p = (U64)ptr / MALLOC_BLOCK_SIZE;
    return (alloc_bitmap[p / 4] & (1 << (p % 4 * 2))) != 0;
}

void memset(void *s, U8 c, U64 n) {
    for (U64 i = 0; i < n; i++) {
        ((U8*)s)[i] = c;
    }
}

void memcpy(void *dest, void *src, U64 n) {
    for (U64 i = 0; i < n; i++) {
        ((U8*)dest)[i] = ((U8*)src)[i];
    }
}
