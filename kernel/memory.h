#pragma once

#define MALLOC_BLOCK_SIZE 16

extern U64 heap_size;
extern U64 mem_free;
extern U8 *alloc_bitmap;
extern U64 last_allocated;

void *malloc(U64 length);
void *calloc(U64 length);
void free(void *ptr);
U64 is_allocated(void *ptr);
void memset(void *s, U8 c, U64 n);
void memcpy(void *dest, void *src, U64 n);
