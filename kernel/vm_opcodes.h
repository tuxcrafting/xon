#pragma once

#define FAULT_UNDEFINED 0x00
#define FAULT_STACK 0x01
#define FAULT_ARITHMETIC 0x02
#define FAULT_KEY 0x03
#define FAULT_TYPE 0x04
#define FAULT_MEMORY 0x05
#define FAULT_INSTRUCTION 0x06
#define FAULT_BOUND 0x07
#define FAULT_PERMISSION 0x08

#define PERM_NATIVE_CTL 0x01
#define PERM_PERM_CTL 0x02

// ->
// Exit the process
#define SYSCALL_EXIT 0x00
// -> pid
// Fork the current process
// A PID of 0 means it's the child process, otherwise it's the parent
#define SYSCALL_FORK 0x01
// bin ->
// Replace the current process's bytecode
#define SYSCALL_EXEC 0x02
// -> pid
// Get the current PID
#define SYSCALL_GETPID 0x03
// obj pid ->
// Send an object to another process
#define SYSCALL_IPC_SEND 0x08
// timeout -> ( obj pid 1 ) OR ( 0 )
// Wait until an object is received or the timeout expires
// The timeout is in milliseconds (negative values never expire)
#define SYSCALL_IPC_RECV 0x09
// length init -> {...}
// Allocate a tuple with a specific length filled with init
#define SYSCALL_ALLOC_TUPLE 0x0A
// length -> "..."
// Allocate a bytestring with a specific length
#define SYSCALL_ALLOC_BYTESTRING 0x0B
// arguments code -> rax
// Execute raw x86 machine code (must be position independent)
// Interrupts are disabled while calling the function, and it is assumed
// to respect the System V ABI
// The code is a bytestring, the arguments are a tuple
// (Arguments are passed as one pointer to the U64 array)
// Requires NATIVE_CTL permission
#define SYSCALL_EXEC_ASM 0x20
// base length -> "..."
// Create a bytestring that maps a specific region of memory
// Requires NATIVE_CTL permission
#define SYSCALL_MAP_STRING 0x21
// pid -> perms
// Gets the permission mask of a process
// If the pid is 0, get the permission mask of the current process
// Requires PERM_CTL permission (except if the pid is 0)
#define SYSCALL_GET_PERMS 0x28
// perms pid ->
// Sets the permission mask of a process
// Requires PERM_CTL permission
#define SYSCALL_SET_PERMS 0x29

// NOP
// ->
// Do nothing
#define OP_NOP 0x00
// DROP
// x ->
// Pop the top of stack
#define OP_DROP 0x01
// DUP
// x -> x x
// Duplicate the top of stack
#define OP_DUP 0x02
// SWAP <amount:varint>
// x y ... -> ... y x
// Swap a specified amount of items in the stack
#define OP_SWAP 0x03
// ENTER <locals:varint>
// ->
// Enter a function (allocate a stack frame for a certain number of local
// variables)
#define OP_ENTER 0x04
// LEAVE
// ->
// Leave a function (return to caller)
#define OP_LEAVE 0x05
// JUMP <displacement:varint>
// ->
// Jump to an opcode (0 displacement is the current instruction)
#define OP_JUMP 0x06
// CALL
// x ->
// Call a function object
#define OP_CALL 0x07
// JUMP_TRUE <displacement:varint>
// x ->
// Same thing as JUMP, but only jumps if the top of stack is true
#define OP_JUMP_TRUE 0x08
// JUMP_TRUE <displacement:varint>
// x ->
// Same thing as JUMP, but only jumps if the top of stack is false
#define OP_JUMP_FALSE 0x09
// SYSCALL <number:varint>
// Call a syscall. The amount of items popped and put on the stack
// depends on the syscall.
#define OP_SYSCALL 0x0F
// PUSH_GLOBAL <index:varint>
// -> x
// Push the value of a global variable
#define OP_PUSH_GLOBAL 0x10
// SET_GLOBAL <index:varint>
// x ->
// Set the value of a global variable
#define OP_SET_GLOBAL 0x11
// PUSH_LOCAL <index:varint>
// -> x
// Push the value of a local variable
#define OP_PUSH_LOCAL 0x12
// SET_LOCAL <index:varint>
// x ->
// Set the value of a local variable
#define OP_SET_LOCAL 0x13
// PUSH_CLOCAL <index:varint> <closure:varint>
// -> x
// Push the value of a local variable from a specified closure
// (closure index 0 is the current closure)
#define OP_PUSH_CLOCAL 0x14
// PUSH_INT <n:varint>
// -> x
// Push an integer
#define OP_PUSH_INT 0x20
// PUSH_FLOAT <n:varint>
// -> x
// Push an IEEE-754 double-precision float
#define OP_PUSH_FLOAT 0x21
// CONS
// x y -> (x y)
// Make a pair of items
#define OP_CONS 0x22
// TUPLE <length:varint>
// x y... -> {x y...}
// Make a tuple
#define OP_TUPLE 0x23
// BYTESTRING <length:varint> <data:bytestring>
// -> "xyz..."
// Make a bytestring
#define OP_BYTESTRING 0x24
// FUNC <displacement:varint>
// -> [a c0 c1...]
// Make a function
#define OP_FUNC 0x25
// PLACEHOLDER
// -> ...
// Placeholder, used for pattern matching
#define OP_PLACEHOLDER 0x26
// HASHMAP
// -> h
// Create an empty hashmap
#define OP_HASHMAP 0x27
// ADD
// x y -> x+y
// Add two values
#define OP_ADD 0x30
// SUB
// x y -> x-y
// Subtrace two values
#define OP_SUB 0x31
// MUL
// x y -> x*y
// Multiply two values
#define OP_MUL 0x32
// DIV
// x y -> x/y
// Divide two values, return the quotient
#define OP_DIV 0x33
// MOD
// x y -> x%y
// Divide two values, return the remainder
#define OP_MOD 0x34
// NEG
// x -> -x
// Negate a value
#define OP_NEG 0x35
// INV
// x -> ~x
// Invert a value
#define OP_INV 0x36
// AND
// x y -> x&y
// AND two values
#define OP_AND 0x37
// OR
// x y -> x|y
// OR two values
#define OP_OR 0x38
// XOR
// x y -> x^y
// XOR two values
#define OP_XOR 0x39
// SHL
// x y -> x<<y
// Shift left a value
#define OP_SHL 0x3A
// SHR
// x y -> x>>y
// Shift right a value
#define OP_SHR 0x3B
// SHAR
// x y -> x>>>y
// Arithmetic shift right a value
#define OP_SHAR 0x3C
// EQ
// x y -> x==y
// Returns if two values are equal
#define OP_EQ 0x40
// NEQ
// x y -> x!=y
// Returns if two values are not equal
#define OP_NEQ 0x41
// LT
// x y -> x<y
// Returns if a value is less than another one
#define OP_LT 0x42
// GT
// x y -> x>y
// Returns if a value is more than another one
#define OP_GT 0x43
// LE
// x y -> x<=y
// Returns if a value is less than or equal to another one
#define OP_LE 0x44
// GE
// x y -> x>=y
// Returns if a value is more than or equal to another one
#define OP_GE 0x45
// BOOL
// x -> bool(x)
// Returns 1 if the top of the stack is truthy, 0 otherwise
#define OP_BOOL 0x46
// BOOL_NOT
// x -> !bool(x)
// Returns 0 if the top of the stack is truthy, 1 otherwise
#define OP_BOOL_NOT 0x47
// CAR
// (x y) -> x
// First element of a pair
#define OP_CAR 0x50
// CDR
// (x y) -> y
// Second element of a pair
#define OP_CDR 0x51
// INDEX
// {x y...} i -> {x y...}(i)
// Index a tuple, a bytestring or a hashmap
#define OP_INDEX 0x52
// LENGTH
// {x y...} -> len({x y...})
// Length of a tuple or a bytestring
#define OP_LENGTH 0x53
// UNPACK
// {x y...} -> x y...
// Unpack a tuple
#define OP_UNPACK 0x54
// HASH
// x -> hash(x)
// Return the hash of an object
#define OP_HASH 0x56
// SET_INDEX
// v {x y...} i -> {x y...}(i)=v
// Set an index in a tuple, a bytestring or a hashmap
#define OP_SET_INDEX 0x57
// ITOF
// int(x) -> float(x)
// Convert an integer to a float
#define OP_ITOF 0x58
// FTOI
// float(x) -> int(x)
// Convert a float to an integer
#define OP_FTOI 0x59
// MATCH
// {x y...} {z w...} -> ( {a b...} 1 ) OR ( 0 )
// Match does pattern matching
// The first popped element is the pattern, and the second is the data
// If the pattern matched, a tuple of all the placeholders replaced by the
// matched data is returned
#define OP_MATCH 0x5A
// SLICE
// {x y...} s e -> {x y...}(s:e)
// Slice a tuple or bytestring
// The returned object is a window and not a copy
// The range is inclusive-exclusive
#define OP_SLICE 0x5B
// GET_TYPE
// x -> type(x)
// Get the type of an object
#define OP_GET_TYPE 0x5C
// TRY
// try catch ->
// Execute the try function, and in case an error happens, execute the catch
// function
#define OP_TRY 0x60
// TRY_CLEANUP
// ->
// Cleanup the internal VM state after a TRY
#define OP_TRY_CLEANUP 0x61
