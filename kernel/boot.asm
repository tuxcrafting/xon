global _start

extern kstage1, kstage2

MODULEALIGN equ 1 << 0
MEMINFO equ 1 << 1
FLAGS equ MODULEALIGN | MEMINFO
MAGIC equ 0x1BADB002
CHECKSUM equ -(MAGIC + FLAGS)

section .text
[bits 32]
align 4
dd MAGIC
dd FLAGS
dd CHECKSUM

_start:
    cli
    mov esp, stack_top
    push 0
    push ebx

    ; detect cpuid
    mov dword [error_ptr], error_msg_cpuid
    pushfd
    pop eax
    mov ecx, eax
    xor eax, 1 << 21
    push eax
    popfd
    pushfd
    pop eax
    push ecx
    popfd
    xor eax, ecx
    jz error

    ; detect extended cpuid
    mov dword [error_ptr], error_msg_ext_cpuid
    mov eax, 0x80000000
    cpuid
    cmp eax, 0x80000001
    jb error

    ; detect long mode
    mov dword [error_ptr], error_msg_lm
    mov eax, 0x80000001
    cpuid
    and edx, 1 << 29
    jz error

    ; disable paging
    mov eax, cr0
    and eax, ~(1 << 31)
    mov cr0, eax

    ; clear page tables, set page table location
    mov edi, pt_pml4
    mov cr3, edi
    xor eax, eax
    mov ecx, 4096
    rep stosd

    ; setup page tables
    mov eax, pt_pdp
    or eax, 3
    mov [pt_pml4], eax
    add eax, 0x1000
    mov [pt_pdp], eax
    mov edi, pt_pd
    mov ecx, 512
    mov eax, 3 | (1 << 7)
pd_loop:
    mov [edi], eax
    add eax, 0x200000
    add edi, 8
    loop pd_loop

    ; switch to long mode
    mov eax, cr4
    or eax, 1 << 5
    mov cr4, eax
    mov ecx, 0xC0000080
    rdmsr
    or eax, 1 << 8
    wrmsr
    mov eax, cr0
    or eax, 1 << 31
    mov cr0, eax

    ; enter 64-bit mode
    lgdt [gdtr]
    jmp gdt64.code:cnt64

error:
    mov esi, [error_ptr]
    xor edi, edi
.loop:
    mov al, [esi]
    or al, al
    jz _end
    or ax, 0x4700
    mov [0xB8000+edi], ax
    inc esi
    add edi, 2
    jmp .loop

_end:
    hlt
    jmp _end

[bits 64]
cnt64:
    mov ax, gdt64.data
    mov ds, ax
    mov es, ax
    mov gs, ax
    mov gs, ax
    mov ss, ax
    pop rdi
    call kstage1
    mov rsp, rax
    ; reuse page table for fs
    mov eax, pt_pml4
    xor edx, edx
    mov ecx, 0xC0000100
    jmp kstage2
[bits 32]

section .data
align 4
error_ptr: dd 0
error_msg_cpuid: db "CPUID not supported!", 0
error_msg_ext_cpuid: db "Extended CPUID not supported!", 0
error_msg_lm: db "Long mode not supported!", 0

align 8
gdt64:
.null: equ $ - gdt64
    dw 0xFFFF
    dw 0
    db 0
    db 0
    db 1
    db 0
.code: equ $ - gdt64
    dw 0
    dw 0
    db 0
    db 10011010b
    db 10101111b
    db 0
.data: equ $ - gdt64
    dw 0
    dw 0
    db 0
    db 10010010b
    db 00000000b
    db 0
gdtr:
    dw $ - gdt64 - 1
    dd gdt64
    dd 0

section .bss
align 16
stack_bottom:
    resb 16384
stack_top:

align 4096
pt_pml4:
    resb 4096
pt_pdp:
    resb 4096
pt_pd:
    resb 4096
.end:
