#include "types.h"
#include "asm.h"
#include "vm.h"
#include "vm_opcodes.h"

vm_t *vm;
vm_t *vm_list[MAX_PIDS];
U64 max_pid, cur_pid;

#ifdef STANDALONE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#undef NULL
#define NULL 0
#define puts _vm_puts
void _vm_puts(U8 *s) {
    printf("%s", s);
}
int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <file>\n", argv[0]);
        return 1;
    }
    FILE *f = fopen(argv[1], "rb");
    if (f == NULL) {
        fprintf(stderr, "Couldn't open file");
        return 1;
    }
    fseek(f, 0, SEEK_END);
    U64 bufsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    U8 *buf = malloc(bufsize);
    if (buf == NULL) {
        fprintf(stderr, "Couldn't allocate buffer");
        fclose(f);
        return 1;
    }
    fread(buf, 1, bufsize, f);
    fclose(f);
    vm = calloc(sizeof(vm_t), 1);
    xvm_create(buf);
    free(buf);
    while ((vm->flags & VF_HALTED) == 0) {
        xvm_step();
    }
    return 0;
}
#define calloc(n) calloc(n, 1)
#else
#include "memory.h"
#include "video.h"

inline void xvm_error(U64 errcode) {
    asm volatile("int $0x60" :: "a"(errcode));
}
#endif

extern void kerror(U8*);

// FNV64 hash
// used for hashing strings for global names etc
U64 FNV_offset = 0xCBF29CE484222325;
U64 FNV_prime = 0x100000001B3;
U64 hash_bytes(U64 len, U8 *b) {
    U64 hash = FNV_offset;
    for (U64 i = 0; i < len; i++) {
        hash = (hash * FNV_prime) ^ b[i];
    }
    return hash;
}

#define refcnt_of(data) ((vm_int_t*)get_ptr_value(data))->refcnt

// ----- pointer creation helpers -----
inline U64 make_int_t(S64 n) {
    vm_int_t *ptr = gc_calloc(sizeof(vm_int_t), T_INT);
    ptr->n = n;
    return make_ptr(ptr, T_INT);
}
inline U64 make_float_t(F64 n) {
    vm_float_t *ptr = gc_calloc(sizeof(vm_float_t), T_FLOAT);
    ptr->n = n;
    return make_ptr(ptr, T_FLOAT);
}
inline U64 make_cons_t(U64 car, U64 cdr) {
    vm_cons_t *ptr = gc_calloc(sizeof(vm_float_t), T_CONS);
    refcnt_of(car)++;
    ptr->car = car;
    refcnt_of(cdr)++;
    ptr->cdr = cdr;
    return make_ptr(ptr, T_CONS);
}

// ----- stack -----
stack_t *stack_push(stack_t **stack, U64 data) {
    stack_t *s = malloc(sizeof(stack_t));
    if (s == NULL) {
        return NULL;
    }
    s->data = data;
    s->previous = *stack;
    *stack = s;
    return s;
}
void xvm_stack_push(stack_t **stack, U64 data) {
    if (data == NULL) {
        xvm_error(FAULT_UNDEFINED);
    }
    if (stack_push(stack, data) == NULL) {
        xvm_gc();
        xvm_error(FAULT_STACK);
    }
    refcnt_of(data)++;
}

U64 stack_pop(stack_t **stack) {
    stack_t *s = *stack;
    if (s == NULL) {
        return NULL;
    }
    U64 data = s->data;
    *stack = s->previous;
    free(s);
    return data;
}
U64 xvm_stack_pop(stack_t **stack) {
    U64 data = stack_pop(stack);
    if (data == NULL) {
        xvm_error(FAULT_STACK);
    }
    refcnt_of(data)--;
    return data;
}

U64 stack_depth(stack_t *stack) {
    U64 n = 0;
    while (stack != NULL) {
        n++;
        stack = stack->previous;
    }
    return n;
}
stack_t *stack_clone(stack_t *src) {
    stack_t *root = NULL;
    stack_t **tmp;
    while (src != NULL) {
        if (root == NULL) {
            stack_push(&root, src->data);
            tmp = &root;
        } else {
            stack_push(tmp, src->data);
        }
        tmp = &(*tmp)->previous;
        src = src->previous;
    }
    return root;
}
stack_t *xvm_stack_clone(stack_t *src) {
    stack_t *root = NULL;
    stack_t **tmp;
    while (src != NULL) {
        refcnt_of(src->data)++;
        if (root == NULL) {
            stack_push(&root, src->data);
            tmp = &root;
        } else {
            stack_push(tmp, src->data);
        }
        tmp = &(*tmp)->previous;
        src = src->previous;
    }
    return root;
}

// ----- hashmap -----
hashtree_t *hashmap_find(hashtree_t *tree, U64 hash, U64 create) {
    for (U64 i = 0; i < 16; i++) {
        U64 b = (hash >> (i * 4)) & 15;
        if (tree->branch[b] == NULL) {
            if (create) {
                tree->branch[b] = calloc(sizeof(hashtree_t));
                tree = tree->branch[b];
            } else {
                return NULL;
            }
        } else {
            tree = tree->branch[b];
        }
    }
    return tree;
}

void hashmap_set(hashtree_t *tree, U64 key, U64 value) {
    U64 khash = hash_obj(key);
    tree = hashmap_find(tree, khash, 1);
    stack_t **k = &tree->keys;
    stack_t **v = &tree->values;
    while (*k != NULL) {
        if (is_equal(key, (*k)->data)) {
            refcnt_of(value)++;
            refcnt_of((*v)->data)--;
            (*v)->data = value;
            return;
        }
        k = &(*k)->previous;
        v = &(*v)->previous;
    }
    xvm_stack_push(k, key);
    xvm_stack_push(v, value);
}
U64 hashmap_get(hashtree_t *tree, U64 key) {
    U64 khash = hash_obj(key);
    tree = hashmap_find(tree, khash, 0);
    if (tree == NULL) {
        return NULL;
    }
    stack_t *k = tree->keys;
    stack_t *v = tree->values;
    while (k != NULL) {
        if (is_equal(key, k->data)) {
            return v->data;
        }
        k = k->previous;
        v = v->previous;
    }
    return NULL;
}

// ----- garbage collector -----
void gc_free_hashtree(hashtree_t *tree, U64 depth) {
    if (depth == 16) {
        while (tree->keys != NULL) {
            xvm_stack_pop(&tree->keys);
            xvm_stack_pop(&tree->values);
        }
        return;
    }
    for (U64 i = 0; i < 16; i++) {
        if (tree->branch[i] != NULL) {
            gc_free_hashtree(tree->branch[i], depth + 1);
        }
    }
    free(tree);
}

stack_t *gc_root;
void gc_free(U64 ptr) {
    if (ptr == NULL) {
        return;
    }
    void *p = get_ptr_value(ptr);
    switch (get_ptr_tag(ptr)) {
    case T_CONS:
        refcnt_of(((vm_cons_t*)p)->car)--;
        refcnt_of(((vm_cons_t*)p)->cdr)--;
        break;
    case T_TUPLE: {
        vm_tuple_t *tuple = p;
        if (tuple->parent == NULL) {
            for (U64 i = 0; i < tuple->len; i++) {
                refcnt_of(tuple->items[i])--;
            }
            free(tuple->items);
        } else {
            refcnt_of(tuple->parent)--;
        }
        break;
    }
    case T_BYTESTRING: {
        vm_bytestring_t *bs = p;
        if (bs->parent == NULL) {
            if ((bs->flags & TF_NOGC_DATA) == 0) {
                free(bs->data);
            }
        } else {
            refcnt_of(bs->parent)--;
        }
        break;
    }
    case T_FUNCTION:
        for (U64 i = 0; i < ((vm_function_t*)p)->len; i++) {
            refcnt_of(((vm_function_t*)p)->scopes[i])--;
        }
        free(((vm_function_t*)p)->scopes);
        break;
    case T_HASHMAP:
        gc_free_hashtree(((vm_hashmap_t*)p)->tree, 0);
        break;
    default:
        break;
    }
    free(p);
}

void gc_sweep(stack_t **stack) {
    while (*stack != NULL) {
        U64 ptr = (*stack)->data;
        if (refcnt_of(ptr) <= 0) {
            gc_free(ptr);
            stack_pop(stack);
            continue;
        }
        if (*stack == NULL) {
            break;
        }
        stack = &((*stack)->previous);
    }
}

U64 gc_allocations;
void *gc_malloc(U64 size, U8 tag) {
    if (gc_allocations++ >= 250) {
        xvm_gc();
        gc_allocations = 0;
    }
    void *p = malloc(size);
    U64 tp = make_ptr(p, tag);
    stack_push(&gc_root, tp);
    return p;
}
void *gc_calloc(U64 size, U8 tag) {
    void *p = gc_malloc(size, tag);
    memset(p, 0, size);
    return p;
}

// ----- multiprocessing -----
U64 xvm_create_process() {
    for (U64 i = 1; i < MAX_PIDS; i++) {
        if (vm_list[i] == NULL) {
            vm_list[i] = malloc(sizeof(vm_t));
            if (i > max_pid) {
                max_pid = i;
            }
            return i;
        }
    }
    return 0;
}
void xvm_reset(vm_t *vm) {
    free(vm->globals);
    free(vm->bytecode);
    while (vm->data_stack) {
        xvm_stack_pop(&vm->data_stack);
    }
    while (vm->return_stack) {
        stack_pop(&vm->return_stack);
    }
    while (vm->locals_stack) {
        xvm_stack_pop(&vm->locals_stack);
    }
    while (vm->catch_stack) {
        xvm_stack_pop(&vm->catch_stack);
    }
    while (vm->ipc_data) {
        xvm_stack_pop(&vm->ipc_data);
    }
    while (vm->ipc_pid) {
        xvm_stack_pop(&vm->ipc_pid);
    }
}
void xvm_destroy_process(U64 pid) {
    if (vm_list[pid] == NULL) {
        return;
    }
    xvm_reset(vm_list[pid]);
    free(vm_list[pid]);
    vm_list[pid] = NULL;
    U64 rmax = 0;
    for (U64 i = 0; i <= max_pid; i++) {
        if (vm_list[i] != NULL) {
            rmax = i;
        }
    }
    max_pid = rmax;
}
void xvm_clone(vm_t *target) {
    target->flags = vm->flags;
    target->permissions = vm->permissions;
    target->pc = vm->pc;
    target->bytecode_len = vm->bytecode_len;
    target->globals_len = vm->globals_len;
    target->bytecode = malloc(vm->bytecode_len);
    memcpy(target->bytecode, vm->bytecode, vm->bytecode_len);
    target->globals = malloc(sizeof(U64) * vm->globals_len);
    memcpy(target->globals, vm->globals, sizeof(U64) * vm->globals_len);
    target->data_stack = xvm_stack_clone(vm->data_stack);
    target->return_stack = stack_clone(vm->return_stack);
    target->locals_stack = xvm_stack_clone(vm->locals_stack);
    stack_t *s = target->locals_stack;
    while (s != NULL) {
        vm_tuple_t *t = get_ptr_value(s->data);
        for (U64 i = 0; i < t->len; i++) {
            refcnt_of(t->items[i])++;
        }
    }
    target->catch_stack = xvm_stack_clone(vm->catch_stack);
    target->ipc_data = xvm_stack_clone(vm->ipc_data);
    target->ipc_pid = xvm_stack_clone(vm->ipc_pid);
}
void xvm_schedule() {
    U64 pid = cur_pid;
    for (U64 i = 1; i <= max_pid; i++) {
        pid++;
        if (pid > max_pid) {
            pid = 1;
        }
        if (vm_list[pid] != NULL) {
            if ((vm_list[pid]->flags & 1) == 0) {
                cur_pid = pid;
                vm = vm_list[pid];
                break;
            }
        }
    }
}

// ----- executable parsing -----
void xvm_next_chunk(chunk_t *chunk, U8 **data) {
    U8 *ptr = *data;
    chunk->name = *(U32*)ptr;
    ptr += 4;
    U64 len = chunk->len = *(U32*)ptr;
    ptr += 4;
    chunk->data = ptr;
    ptr += len;
    if (len % 4 != 0) {
        ptr += 4 - len % 4;
    }
    *data = ptr;
}

U64 xvm_create(U8 *exe) {
    chunk_t chunk;
    xvm_next_chunk(&chunk, &exe);
    if (chunk.name != s_to_i(U32, "\x55xvm")) {
        return 1;
    }
    exe = chunk.data;
    U8 *max = exe + chunk.len;
    while (exe < max) {
        xvm_next_chunk(&chunk, &exe);
        if (chunk.name == s_to_i(U32, "code")) {
            vm->bytecode = malloc(chunk.len);
            memcpy(vm->bytecode, chunk.data, chunk.len);
            vm->bytecode_len = chunk.len;
        } else if (chunk.name == s_to_i(U32, "glob")) {
            vm->globals_len = chunk.len / 8;
            vm->globals = calloc(sizeof(U64) * vm->globals_len);
        }
    }
    vm->pc = 0;
    vm->data_stack = NULL;
    vm->return_stack = NULL;
    vm->locals_stack = NULL;
    return 0;
}

// ----- actual vm -----
U8 xvm_fetch() {
    if (vm->pc >= vm->bytecode_len) {
        xvm_error(FAULT_UNDEFINED);
    }
    return vm->bytecode[vm->pc++];
}

S64 xvm_fetch_varint() {
    S64 n = 0;
    U64 i = 0;
    for (;;) {
        U8 b = xvm_fetch();
        if (b & 0x80) {
            n |= (U64)(b & 0x7F) << i;
            i += 7;
        } else {
            n |= (U64)(b & 0x3F) << i;
            if (b & 0x40) {
                n = -n;
            }
            break;
        }
    }
    return n;
}

inline void assert_type(U64 p, U8 t) {
    if (get_ptr_tag(p) != t) {
        xvm_error(FAULT_TYPE);
    }
}
inline void assert_permissions(U32 p) {
    if ((vm->permissions & p) != p) {
        xvm_error(FAULT_PERMISSION);
    }
}

U64 as_bool(U64 ptr) {
    void *p = get_ptr_value(ptr);
    switch (get_ptr_tag(ptr)) {
    case T_INT:
        return ((vm_int_t*)p)->n != 0;
    case T_FLOAT:
        return ((vm_float_t*)p)->n != 0.0;
    case T_CONS:
        return 0;
    case T_TUPLE:
        return ((vm_tuple_t*)p)->len != 0;
    case T_BYTESTRING:
        return ((vm_bytestring_t*)p)->len != 0;
    default:
        return 0;
    }
}
U64 is_equal(U64 a, U64 b) {
    if (get_ptr_tag(a) != get_ptr_tag(b)) {
        return 0;
    }
    void *ap = get_ptr_value(a);
    void *bp = get_ptr_value(b);
    switch (get_ptr_tag(a)) {
    case T_INT:
        return ((vm_int_t*)ap)->n == ((vm_int_t*)bp)->n;
    case T_FLOAT:
        return ((vm_float_t*)ap)->n == ((vm_float_t*)bp)->n;
    case T_CONS: {
        vm_cons_t *av = ap;
        vm_cons_t *bv = bp;
        return is_equal(av->car, bv->car) && is_equal(av->cdr, bv->cdr);
    }
    case T_TUPLE: {
        vm_tuple_t *av = ap;
        vm_tuple_t *bv = bp;
        if (av->len != bv->len) {
            return 0;
        }
        for (U64 i = 0; i < av->len; i++) {
            if (!is_equal(av->items[i], bv->items[i])) {
                return 0;
            }
        }
        return 1;
    }
    case T_BYTESTRING: {
        vm_bytestring_t *av = ap;
        vm_bytestring_t *bv = bp;
        if (av->len != bv->len) {
            return 0;
        }
        for (U64 i = 0; i < av->len; i++) {
            if (av->data[i] != bv->data[i]) {
                return 0;
            }
        }
        return 1;
    }
    default:
        return 0;
    }
}

U64 hash_obj(U64 obj) {
    void *p = get_ptr_value(obj);
    switch (get_ptr_tag(obj)) {
    case T_INT:
        return ((vm_int_t*)p)->n;
    case T_FLOAT: {
        union {
            U64 i;
            F64 f;
        } n;
        n.f = ((vm_int_t*)p)->n;
        return n.i;
    }
    case T_CONS:
        return hash_obj(((vm_cons_t*)p)->car) * FNV_prime
             ^ hash_obj(((vm_cons_t*)p)->cdr);
    case T_TUPLE: {
        U64 h = FNV_offset;
        vm_tuple_t *tuple = p;
        for (U64 i = 0; i < tuple->len; i++) {
            h = (h * FNV_prime) ^ hash_obj(tuple->items[i]);
        }
        return h;
    }
    case T_BYTESTRING:
        return hash_bytes(((vm_bytestring_t*)p)->len,
                ((vm_bytestring_t*)p)->data);
    default:
        return obj;
    }
}

struct pattern_match {
    U64 success;
    U64 var_num;
    U64 *vars;
};

struct pattern_match pattern_match(U64 data, U64 pattern) {
    struct pattern_match match = {0, 0, NULL};
    if (get_ptr_tag(pattern) == T_PLACEHOLDER) {
        match.success = 1;
        match.var_num = 1;
        match.vars = malloc(sizeof(U64));
        match.vars[0] = data;
        return match;
    }
    if (get_ptr_tag(pattern) != get_ptr_tag(data)) {
        return match;
    }
    switch (get_ptr_tag(pattern)) {
    case T_INT:
    case T_FLOAT:
    case T_BYTESTRING:
        match.success = is_equal(data, pattern);
        break;
    case T_CONS: {
        vm_cons_t *d_v = get_ptr_value(data);
        vm_cons_t *p_v = get_ptr_value(pattern);
        struct pattern_match car = pattern_match(d_v->car, p_v->car);
        struct pattern_match cdr = pattern_match(d_v->cdr, p_v->cdr);
        if (car.success && cdr.success) {
            match.success = 1;
            match.var_num = car.var_num + cdr.var_num;
            if (match.var_num) {
                match.vars = malloc(sizeof(U64) * match.var_num);
                memcpy(match.vars, car.vars, sizeof(U64) * car.var_num);
                memcpy(match.vars + car.var_num,
                        cdr.vars, sizeof(U64) * cdr.var_num);
            }
        }
        free(car.vars);
        free(cdr.vars);
        break;
    }
    case T_TUPLE: {
        vm_tuple_t *d_v = get_ptr_value(data);
        vm_tuple_t *p_v = get_ptr_value(pattern);
        if (d_v->len != p_v->len) {
            break;
        }
        for (U64 i = 0; i < d_v->len; i++) {
            struct pattern_match m = pattern_match(d_v->items[i],
                    p_v->items[i]);
            if (!m.success) {
                free(match.vars);
                return match;
            }
            if (m.var_num) {
                U64 old_n = match.var_num;
                match.var_num += m.var_num;
                U64 *new_ptr = malloc(sizeof(U64) * match.var_num);
                memcpy(new_ptr, match.vars, sizeof(U64) * old_n);
                memcpy(new_ptr + old_n, m.vars, sizeof(U64) * m.var_num);
                free(match.vars);
                match.vars = new_ptr;
            }
        }
        match.success = 1;
        break;
    }
    default:
        break;
    }
    return match;
}

#define vm_num_binop(opu, op) case OP_##opu: {\
        U64 b = xvm_stack_pop(&vm->data_stack); \
        U64 a = xvm_stack_pop(&vm->data_stack); \
        if (get_ptr_tag(a) != get_ptr_tag(b) \
         || !(get_ptr_tag(a) == T_INT || get_ptr_tag(a) == T_FLOAT)) { \
            xvm_error(FAULT_TYPE); \
        } \
        if (get_ptr_tag(a) == T_INT) { \
            vm_int_t *a_v = get_ptr_value(a); \
            vm_int_t *b_v = get_ptr_value(b); \
            a = make_int_t(a_v->n op b_v->n); \
        } else { \
            vm_float_t *a_v = get_ptr_value(a); \
            vm_float_t *b_v = get_ptr_value(b); \
            a = make_float_t(a_v->n op b_v->n); \
        } \
        xvm_stack_push(&vm->data_stack, a); \
    } \
    break;
#define vm_int_binop(opu, op) case OP_##opu: {\
        U64 b = xvm_stack_pop(&vm->data_stack); \
        U64 a = xvm_stack_pop(&vm->data_stack); \
        if (get_ptr_tag(a) != get_ptr_tag(b) \
         || get_ptr_tag(a) != T_INT) { \
            xvm_error(FAULT_TYPE); \
        } \
        vm_int_t *a_v = get_ptr_value(a); \
        vm_int_t *b_v = get_ptr_value(b); \
        xvm_stack_push(&vm->data_stack, make_int_t(a_v->n op b_v->n)); \
    } \
    break;
#define vm_num_unop(opu, op) case OP_##opu: {\
        U64 a = xvm_stack_pop(&vm->data_stack); \
        if (!(get_ptr_tag(a) == T_INT || get_ptr_tag(a) == T_FLOAT)) { \
            xvm_error(FAULT_TYPE); \
        } \
        if (get_ptr_tag(a) == T_INT) { \
            vm_int_t *a_v = get_ptr_value(a); \
            a = make_int_t(op a_v->n); \
        } else { \
            vm_float_t *a_v = get_ptr_value(a); \
            a = make_float_t(op a_v->n); \
        } \
        xvm_stack_push(&vm->data_stack, a); \
    } \
    break;
#define vm_int_unop(opu, op) case OP_##opu: {\
        U64 a = xvm_stack_pop(&vm->data_stack); \
        if (get_ptr_tag(a) != T_INT) { \
            xvm_error(FAULT_TYPE); \
        } \
        vm_int_t *a_v = get_ptr_value(a); \
        xvm_stack_push(&vm->data_stack, make_int_t(op a_v->n)); \
    } \
    break;

void xvm_call(vm_function_t *f) {
    for (U64 i = 0; i < f->len; i++) {
        xvm_stack_push(&vm->locals_stack, f->scopes[f->len - 1 - i]);
    }
    stack_push(&vm->return_stack, vm->pc);
    vm->pc = f->pc;
}

U64 xvm_step_rsp;

void xvm_step() {
    asm volatile("mov %rsp, (xvm_step_rsp);");
    if (vm->flags & VF_IPC_WAIT) {
        return;
    }
    U64 pc = vm->pc;
    switch (xvm_fetch()) {
    case OP_NOP:
        examine1(xvm_stack_pop(&vm->data_stack));
        break;
    case OP_DROP: {
        xvm_stack_pop(&vm->data_stack);
        break;
    }
    case OP_DUP: {
        U64 x = xvm_stack_pop(&vm->data_stack);
        xvm_stack_push(&vm->data_stack, x);
        xvm_stack_push(&vm->data_stack, x);
        break;
    }
    case OP_SWAP: {
        U64 n = xvm_fetch_varint();
        U64 *buf = malloc(sizeof(U64) * n);
        for (U64 i = 0; i < n; i++) {
            buf[i] = xvm_stack_pop(&vm->data_stack);
        }
        for (U64 i = 0; i < n; i++) {
            xvm_stack_push(&vm->data_stack, buf[i]);
        }
        free(buf);
        break;
    }
    case OP_ENTER: {
        vm_tuple_t *locals = gc_calloc(sizeof(vm_tuple_t), T_TUPLE);
        locals->len = xvm_fetch_varint();
        locals->items = calloc(sizeof(U64) * locals->len);
        xvm_stack_push(&vm->locals_stack, make_ptr(locals, T_TUPLE));
        break;
    }
    case OP_LEAVE: {
        xvm_stack_pop(&vm->locals_stack);
        if (vm->return_stack == NULL) {
            xvm_error(FAULT_STACK);
        }
        vm->pc = stack_pop(&vm->return_stack);
        break;
    }
    case OP_JUMP: {
        vm->pc = pc + xvm_fetch_varint();
        break;
    }
    case OP_CALL: {
        U64 a = xvm_stack_pop(&vm->data_stack);
        assert_type(a, T_FUNCTION);
        xvm_call(get_ptr_value(a));
        break;
    }
    case OP_JUMP_TRUE: {
        S64 disp = xvm_fetch_varint();
        if (as_bool(xvm_stack_pop(&vm->data_stack)) != 0) {
            vm->pc = pc + disp;
        }
        break;
    }
    case OP_JUMP_FALSE: {
        S64 disp = xvm_fetch_varint();
        if (as_bool(xvm_stack_pop(&vm->data_stack)) == 0) {
            vm->pc = pc + disp;
        }
        break;
    }
    case OP_SYSCALL: {
        switch (xvm_fetch_varint()) {
        case SYSCALL_EXIT:
            xvm_destroy_process(cur_pid);
            break;
        case SYSCALL_FORK: {
            U64 pid = xvm_create_process();
            if (pid == 0) {
                xvm_error(FAULT_UNDEFINED);
            }
            xvm_clone(vm_list[pid]);
            vm_list[pid]->parent = cur_pid;
            xvm_stack_push(&vm_list[pid]->data_stack, make_int_t(0));
            xvm_stack_push(&vm->data_stack, make_int_t(pid));
            break;
        }
        case SYSCALL_EXEC: {
            U64 s = xvm_stack_pop(&vm->data_stack);
            assert_type(s, T_BYTESTRING);
            xvm_reset(vm);
            xvm_create(((vm_bytestring_t*)get_ptr_value(s))->data);
            break;
        }
        case SYSCALL_GETPID: {
            xvm_stack_push(&vm->data_stack, make_int_t(cur_pid));
            break;
        }
        case SYSCALL_IPC_SEND: {
            U64 pid = xvm_stack_pop(&vm->data_stack);
            assert_type(pid, T_INT);
            U64 pid_obj = make_int_t(cur_pid);
            U64 obj = xvm_stack_pop(&vm->data_stack);
            pid = ((vm_int_t*)get_ptr_value(pid))->n;
            if (pid >= MAX_PIDS || vm_list[pid] == NULL) {
                xvm_error(FAULT_UNDEFINED);
            }
            vm_t *target = vm_list[pid];
            if (target->flags & VF_IPC_WAIT) {
                target->flags &= ~VF_IPC_WAIT;
                xvm_stack_push(&target->data_stack, obj);
                xvm_stack_push(&target->data_stack, pid_obj);
                xvm_stack_push(&target->data_stack, make_int_t(1));
            } else {
                refcnt_of(pid_obj)++;
                refcnt_of(obj)++;
                stack_t **a = &target->ipc_data;
                stack_t **b = &target->ipc_pid;
                while (*a != NULL) {
                    a = &(*a)->previous;
                    b = &(*b)->previous;
                }
                xvm_stack_push(a, obj);
                xvm_stack_push(b, pid_obj);
            }
            break;
        }
        case SYSCALL_IPC_RECV: {
            xvm_stack_pop(&vm->data_stack);
            if (vm->ipc_data == NULL) {
                vm->flags |= VF_IPC_WAIT;
            } else {
                U64 obj = xvm_stack_pop(&vm->ipc_data);
                U64 pid_obj = xvm_stack_pop(&vm->ipc_pid);
                xvm_stack_push(&vm->data_stack, obj);
                xvm_stack_push(&vm->data_stack, pid_obj);
                xvm_stack_push(&vm->data_stack, make_int_t(1));
            }
            break;
        }
        case SYSCALL_ALLOC_TUPLE: {
            U64 init = xvm_stack_pop(&vm->data_stack);
            U64 len = xvm_stack_pop(&vm->data_stack);
            assert_type(len, T_INT);
            vm_tuple_t *tuple = gc_calloc(sizeof(vm_tuple_t), T_TUPLE);
            tuple->len = len;
            tuple->items = malloc(sizeof(U64) * len);
            for (U64 i = 0; i < len; i++) {
                tuple->items[i] = init;
            }
            refcnt_of(init) += len;
            xvm_stack_push(&vm->data_stack, make_ptr(tuple, T_TUPLE));
            break;
        }
        case SYSCALL_ALLOC_BYTESTRING: {
            U64 len = xvm_stack_pop(&vm->data_stack);
            assert_type(len, T_INT);
            vm_bytestring_t* bs = gc_calloc(sizeof(vm_bytestring_t),
                    T_BYTESTRING);
            bs->len = len;
            bs->data = calloc(len);
            xvm_stack_push(&vm->data_stack, make_ptr(bs, T_BYTESTRING));
            break;
        }
        case SYSCALL_EXEC_ASM: {
            assert_permissions(PERM_NATIVE_CTL);
            U64 code = xvm_stack_pop(&vm->data_stack);
            U64 args = xvm_stack_pop(&vm->data_stack);
            assert_type(code, T_BYTESTRING);
            assert_type(args, T_TUPLE);
            union {
                U8 *b;
                U64 (*f)(U64*);
            } u;
            u.b = ((vm_bytestring_t*)get_ptr_value(code))->data;
            cli;
            U64 r = u.f(((vm_tuple_t*)get_ptr_value(args))->items);
            sti;
            xvm_stack_push(&vm->data_stack, make_int_t(r));
            break;
        }
        case SYSCALL_MAP_STRING: {
            assert_permissions(PERM_NATIVE_CTL);
            vm_bytestring_t* bs = gc_calloc(sizeof(vm_bytestring_t),
                    T_BYTESTRING);
            bs->flags |= TF_NOGC_DATA;
            U64 len = xvm_stack_pop(&vm->data_stack);
            U64 base = xvm_stack_pop(&vm->data_stack);
            assert_type(len, T_INT);
            assert_type(base, T_INT);
            bs->data = (U8*)((vm_int_t*)get_ptr_value(base))->n;
            bs->len = ((vm_int_t*)get_ptr_value(base))->n;
            xvm_stack_push(&vm->data_stack, make_ptr(bs, T_BYTESTRING));
            break;
        }
        case SYSCALL_GET_PERMS: {
            U64 pid = xvm_stack_pop(&vm->data_stack);
            assert_type(pid, T_INT);
            pid = ((vm_int_t*)get_ptr_value(pid))->n;
            if (pid == 0) {
                pid = cur_pid;
            } else {
                assert_permissions(PERM_PERM_CTL);
            }
            xvm_stack_push(&vm->data_stack,
                    make_int_t(vm_list[pid]->permissions));
            break;
        }
        case SYSCALL_SET_PERMS: {
            U64 pid = xvm_stack_pop(&vm->data_stack);
            U64 perms = xvm_stack_pop(&vm->data_stack);
            assert_type(pid, T_INT);
            pid = ((vm_int_t*)get_ptr_value(pid))->n;
            perms = ((vm_int_t*)get_ptr_value(perms))->n;
            assert_permissions(PERM_PERM_CTL);
            if ((perms & ~vm->permissions) != 0) {
                xvm_error(FAULT_PERMISSION);
            }
            vm_list[pid]->permissions = perms;
            break;
        }
        default:
            xvm_error(FAULT_INSTRUCTION);
            break;
        }
        break;
    }
    case OP_PUSH_GLOBAL: {
        U64 n = xvm_fetch_varint();
        if (n >= vm->globals_len) {
            xvm_error(FAULT_BOUND);
        }
        xvm_stack_push(&vm->data_stack, vm->globals[n]);
        break;
    }
    case OP_SET_GLOBAL: {
        U64 data = xvm_stack_pop(&vm->data_stack);
        refcnt_of(data)++;
        U64 n = xvm_fetch_varint();
        if (n >= vm->globals_len) {
            xvm_error(FAULT_BOUND);
        }
        U64 old = vm->globals[n];
        if (old != NULL) {
            refcnt_of(old)--;
        }
        vm->globals[n] = data;
        break;
    }
    case OP_PUSH_LOCAL: {
        U64 n = xvm_fetch_varint();
        if (vm->locals_stack == NULL) {
            xvm_error(FAULT_BOUND);
        }
        vm_tuple_t *locals = get_ptr_value(vm->locals_stack->data);
        if (n >= locals->len) {
            xvm_error(FAULT_BOUND);
        }
        U64 p = locals->items[n];
        xvm_stack_push(&vm->data_stack, p);
        break;
    }
    case OP_SET_LOCAL: {
        U64 data = xvm_stack_pop(&vm->data_stack);
        refcnt_of(data)++;
        if (vm->locals_stack == NULL) {
            xvm_error(FAULT_BOUND);
        }
        vm_tuple_t *locals = get_ptr_value(vm->locals_stack->data);
        U64 n = xvm_fetch_varint();
        if (n >= locals->len) {
            xvm_error(FAULT_BOUND);
        }
        U64 old = locals->items[n];
        if (old != NULL) {
            refcnt_of(old)--;
        }
        locals->items[n] = data;
        break;
    }
    case OP_PUSH_CLOCAL: {
        U64 n = xvm_fetch_varint();
        U64 i = xvm_fetch_varint();
        stack_t *s = vm->locals_stack;
        while (i--) {
            if (s == NULL) {
                xvm_error(FAULT_BOUND);
            }
            s = s->previous;
        }
        if (s == NULL) {
            xvm_error(FAULT_BOUND);
        }
        vm_tuple_t *locals = get_ptr_value(s->data);
        if (locals == NULL || n >= locals->len) {
            xvm_error(FAULT_BOUND);
        }
        U64 p = locals->items[n];
        xvm_stack_push(&vm->data_stack, p);
        break;
    }
    case OP_PUSH_INT: {
        xvm_stack_push(&vm->data_stack, make_int_t(xvm_fetch_varint()));
        break;
    }
    case OP_PUSH_FLOAT: {
        union {
            S64 i;
            F64 f;
        } n;
        n.i = xvm_fetch_varint();
        xvm_stack_push(&vm->data_stack, make_float_t(n.f));
        break;
    }
    case OP_CONS: {
        U64 b = xvm_stack_pop(&vm->data_stack);
        U64 a = xvm_stack_pop(&vm->data_stack);
        xvm_stack_push(&vm->data_stack, make_cons_t(a, b));
        break;
    }
    case OP_TUPLE: {
        vm_tuple_t *tuple = gc_calloc(sizeof(vm_tuple_t), T_TUPLE);
        tuple->len = xvm_fetch_varint();
        tuple->items = malloc(sizeof(U64) * tuple->len);
        for (U64 i = 0; i < tuple->len; i++) {
            U64 item = xvm_stack_pop(&vm->data_stack);
            refcnt_of(item)++;
            tuple->items[tuple->len - 1 - i] = item;
        }
        xvm_stack_push(&vm->data_stack, make_ptr(tuple, T_TUPLE));
        break;
    }
    case OP_BYTESTRING: {
        vm_bytestring_t *bs =
            gc_calloc(sizeof(vm_bytestring_t), T_BYTESTRING);
        bs->len = xvm_fetch_varint();
        bs->data = malloc(bs->len);
        for (U64 i = 0; i < bs->len; i++) {
            bs->data[i] = xvm_fetch();
        }
        xvm_stack_push(&vm->data_stack, make_ptr(bs, T_BYTESTRING));
        break;
    }
    case OP_FUNC: {
        U64 depth = stack_depth(vm->locals_stack);
        vm_function_t *f = gc_calloc(sizeof(vm_function_t), T_FUNCTION);
        f->pc = pc + xvm_fetch_varint();
        f->len = depth;
        if (depth) {
            f->scopes = malloc(sizeof(U64) * depth);
            stack_t *s = vm->locals_stack;
            U64 i = 0;
            while (s != NULL) {
                refcnt_of(s->data)++;
                f->scopes[i++] = s->data;
                s = s->previous;
            }
        }
        xvm_stack_push(&vm->data_stack, make_ptr(f, T_FUNCTION));
        break;
    }
    case OP_PLACEHOLDER:
        xvm_stack_push(&vm->data_stack, make_ptr(gc_calloc(
                        sizeof(vm_int_t), T_PLACEHOLDER), T_PLACEHOLDER));
        break;
    case OP_HASHMAP: {
        vm_hashmap_t *hm = gc_calloc(sizeof(vm_hashmap_t), T_HASHMAP);
        hm->tree = calloc(sizeof(hashtree_t));
        xvm_stack_push(&vm->data_stack, make_ptr(hm, T_HASHMAP));
        break;
    }
    vm_num_binop(ADD, +)
    vm_num_binop(SUB, -)
    vm_num_binop(MUL, *)
    vm_num_binop(DIV, /)
    vm_num_unop(NEG, -)
    vm_int_unop(INV, ~)
    vm_int_binop(MOD, %)
    vm_int_binop(AND, &)
    vm_int_binop(OR, |)
    vm_int_binop(XOR, ^)
    vm_int_binop(SHL, <<)
    case OP_SHR: {
        U64 b = xvm_stack_pop(&vm->data_stack);
        U64 a = xvm_stack_pop(&vm->data_stack);
        if (get_ptr_tag(a) != get_ptr_tag(b)
         || get_ptr_tag(a) != T_INT) {
            xvm_error(FAULT_TYPE);
        }
        vm_int_t *a_v = get_ptr_value(a);
        vm_int_t *b_v = get_ptr_value(b);
        xvm_stack_push(&vm->data_stack, make_int_t((U64)a_v->n >> b_v->n));
        break;
    }
    vm_int_binop(SHAR, >>)
    case OP_EQ: {
        xvm_stack_push(&vm->data_stack, make_int_t(is_equal(
                        xvm_stack_pop(&vm->data_stack),
                        xvm_stack_pop(&vm->data_stack))));
        break;
    }
    case OP_NEQ: {
        xvm_stack_push(&vm->data_stack, make_int_t(!is_equal(
                        xvm_stack_pop(&vm->data_stack),
                        xvm_stack_pop(&vm->data_stack))));
        break;
    }
    vm_num_binop(LT, <)
    vm_num_binop(GT, >)
    vm_num_binop(LE, <=)
    vm_num_binop(GE, >=)
    case OP_BOOL: {
        xvm_stack_push(&vm->data_stack,
                make_int_t(as_bool(xvm_stack_pop(&vm->data_stack))));
        break;
    }
    case OP_BOOL_NOT: {
        xvm_stack_push(&vm->data_stack,
                make_int_t(!as_bool(xvm_stack_pop(&vm->data_stack))));
        break;
    }
    case OP_CAR: {
        U64 a = xvm_stack_pop(&vm->data_stack);
        assert_type(a, T_CONS);
        xvm_stack_push(&vm->data_stack,
                ((vm_cons_t*)get_ptr_value(a))->car);
        break;
    }
    case OP_CDR: {
        U64 a = xvm_stack_pop(&vm->data_stack);
        assert_type(a, T_CONS);
        xvm_stack_push(&vm->data_stack,
                ((vm_cons_t*)get_ptr_value(a))->cdr);
        break;
    }
    case OP_INDEX: {
        U64 i = xvm_stack_pop(&vm->data_stack);
        U64 a = xvm_stack_pop(&vm->data_stack);
        void *p = get_ptr_value(a);
        switch (get_ptr_tag(a)) {
        case T_TUPLE: {
            assert_type(i, T_INT);
            i = ((vm_int_t*)get_ptr_value(i))->n;
            vm_tuple_t *tuple = p;
            if (i >= tuple->len) {
                xvm_error(FAULT_KEY);
            }
            xvm_stack_push(&vm->data_stack, tuple->items[i]);
            break;
        }
        case T_BYTESTRING: {
            assert_type(i, T_INT);
            i = ((vm_int_t*)get_ptr_value(i))->n;
            vm_bytestring_t *bs = p;
            if (i >= bs->len) {
                xvm_error(FAULT_KEY);
            }
            xvm_stack_push(&vm->data_stack, make_int_t(bs->data[i]));
            break;
        }
        case T_HASHMAP: {
            U64 v = hashmap_get(((vm_hashmap_t*)p)->tree, i);
            if (v == NULL) {
                xvm_error(FAULT_KEY);
            }
            xvm_stack_push(&vm->data_stack, v);
            break;
        }
        default:
            xvm_error(FAULT_TYPE);
            break;
        }
        break;
    }
    case OP_LENGTH: {
        U64 a = xvm_stack_pop(&vm->data_stack);
        void *p = get_ptr_value(a);
        switch (get_ptr_tag(a)) {
        case T_TUPLE:
            xvm_stack_push(&vm->data_stack, ((vm_tuple_t*)p)->len);
            break;
        case T_BYTESTRING:
            xvm_stack_push(&vm->data_stack,
                    make_int_t(((vm_bytestring_t*)p)->len));
            break;
        default:
            xvm_error(FAULT_TYPE);
            break;
        }
        break;
    }
    case OP_UNPACK: {
        U64 a = xvm_stack_pop(&vm->data_stack);
        assert_type(a, T_TUPLE);
        vm_tuple_t *t = get_ptr_value(a);
        for (U64 i = 0; i < t->len; i++) {
            xvm_stack_push(&vm->data_stack, t->items[t->len - 1 - i]);
        }
        break;
    }
    case OP_HASH: {
        U64 a = xvm_stack_pop(&vm->data_stack);
        xvm_stack_push(&vm->data_stack, make_int_t(hash_obj(a)));
        break;
    }
    case OP_SET_INDEX: {
        U64 i = xvm_stack_pop(&vm->data_stack);
        U64 a = xvm_stack_pop(&vm->data_stack);
        U64 v = xvm_stack_pop(&vm->data_stack);
        void *p = get_ptr_value(a);
        switch (get_ptr_tag(a)) {
        case T_TUPLE: {
            assert_type(i, T_INT);
            i = ((vm_int_t*)get_ptr_value(i))->n;
            vm_tuple_t *tuple = p;
            if (i >= tuple->len) {
                xvm_error(FAULT_KEY);
            }
            if (tuple->items[i] != NULL) {
                refcnt_of(tuple->items[i])--;
            }
            tuple->items[i] = v;
            refcnt_of(v)++;
            break;
        }
        case T_BYTESTRING:
            assert_type(i, T_INT);
            i = ((vm_int_t*)get_ptr_value(i))->n;
            assert_type(v, T_INT);
            vm_bytestring_t *bs = p;
            if (i >= bs->len) {
                xvm_error(FAULT_KEY);
            }
            bs->data[i] = ((vm_int_t*)get_ptr_value(v))->n & 0xFF;
            break;
        case T_HASHMAP: {
            hashmap_set(((vm_hashmap_t*)get_ptr_value(a))->tree, i, v);
            break;
        }
        default:
            xvm_error(FAULT_TYPE);
            break;
        }
        xvm_stack_push(&vm->data_stack, a);
        break;
    }
    case OP_ITOF: {
        U64 a = xvm_stack_pop(&vm->data_stack);
        assert_type(a, T_INT);
        xvm_stack_push(&vm->data_stack, make_float_t(
                    (F64)((vm_int_t*)get_ptr_value(a))->n));
        break;
    }
    case OP_FTOI: {
        U64 a = xvm_stack_pop(&vm->data_stack);
        assert_type(a, T_FLOAT);
        xvm_stack_push(&vm->data_stack, make_int_t(
                    (S64)((vm_float_t*)get_ptr_value(a))->n));
        break;
    }
    case OP_MATCH: {
        U64 pattern = xvm_stack_pop(&vm->data_stack);
        U64 data = xvm_stack_pop(&vm->data_stack);
        struct pattern_match match = pattern_match(data, pattern);
        if (match.success) {
            vm_tuple_t *tuple = gc_calloc(sizeof(vm_tuple_t), T_TUPLE);
            tuple->len = match.var_num;
            tuple->items = malloc(sizeof(U64) * match.var_num);
            for (U64 i = 0; i < match.var_num; i++) {
                tuple->items[i] = match.vars[i];
                refcnt_of(match.vars[i])++;
            }
            free(match.vars);
            xvm_stack_push(&vm->data_stack, make_ptr(tuple, T_TUPLE));
            xvm_stack_push(&vm->data_stack, make_int_t(1));
        } else {
            xvm_stack_push(&vm->data_stack, make_int_t(0));
        }
        break;
    }
    case OP_SLICE: {
        U64 e = xvm_stack_pop(&vm->data_stack);
        assert_type(e, T_INT);
        e = ((vm_int_t*)get_ptr_value(e))->n;
        U64 s = xvm_stack_pop(&vm->data_stack);
        assert_type(s, T_INT);
        s = ((vm_int_t*)get_ptr_value(s))->n;
        U64 a = xvm_stack_pop(&vm->data_stack);
        refcnt_of(a)++;
        void *p = get_ptr_value(a);
        switch (get_ptr_tag(a)) {
        case T_TUPLE: {
            vm_tuple_t *tuple = p;
            if (s >= tuple->len || e > tuple->len) {
                xvm_error(FAULT_KEY);
            }
            vm_tuple_t *wtuple = gc_calloc(sizeof(vm_tuple_t), T_TUPLE);
            wtuple->len = e - s;
            wtuple->items = tuple->items + s;
            wtuple->parent = a;
            xvm_stack_push(&vm->data_stack, make_ptr(wtuple, T_TUPLE));
            break;
        }
        case T_BYTESTRING: {
            vm_bytestring_t *bs = p;
            if (s >= bs->len || e > bs->len) {
                xvm_error(FAULT_KEY);
            }
            vm_bytestring_t *wbs =
                gc_calloc(sizeof(vm_bytestring_t), T_BYTESTRING);
            wbs->len = e - s;
            wbs->data = bs->data + s;
            wbs->parent = a;
            xvm_stack_push(&vm->data_stack, make_ptr(wbs, T_BYTESTRING));
            break;
        }
        default:
            xvm_error(FAULT_TYPE);
            break;
        }
        break;
    }
    case OP_GET_TYPE:
        xvm_stack_push(&vm->data_stack, make_int_t(get_ptr_tag(
                        xvm_stack_pop(&vm->data_stack))));
        break;
    case OP_TRY: {
        U64 c = xvm_stack_pop(&vm->data_stack);
        U64 t = xvm_stack_pop(&vm->data_stack);
        assert_type(c, T_FUNCTION);
        assert_type(t, T_FUNCTION);
        stack_push(&vm->catch_stack, stack_depth(vm->return_stack));
        stack_push(&vm->catch_stack, stack_depth(vm->data_stack));
        xvm_stack_push(&vm->catch_stack, c);
        xvm_call(get_ptr_value(t));
        break;
    }
    case OP_TRY_CLEANUP: {
        if (vm->catch_stack == NULL) {
            break;
        }
        xvm_stack_pop(&vm->catch_stack);
        stack_pop(&vm->catch_stack);
        stack_pop(&vm->catch_stack);
        break;
    }
    default:
        xvm_error(FAULT_INSTRUCTION);
        break;
    }
    asm volatile(".global _xvm_step_end;"
                 "_xvm_step_end:");
}
