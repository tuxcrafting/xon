#pragma once

typedef unsigned char U8;
typedef signed char S8;
typedef unsigned short U16;
typedef signed short S16;
typedef unsigned int U32;
typedef signed int S32;
typedef unsigned long long U64;
typedef signed long long S64;
typedef float F32;
typedef double F64;

#define NULL 0

#define K(x) (x * 1024)
#define M(x) (x * 1024 * 1024)

#define s_to_i(t, s) *(t*)s
