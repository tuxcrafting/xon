#pragma once

#define VRAM ((U16*)0xB8000)

void putchar(U8 c);
void puts(U8 *s);
void cursor_enable(U8 cursor_start, U8 cursor_end);
void cursor_disable();
void cursor_move(U64 x, U64 y);
void itoa(U64 buf_len, U8 *buf, U64 n, U64 base);
