CC=gcc
CFLAGS=-m64 -c -ffreestanding -O2 -Wall -Wextra -Werror -Ikernel \
       -std=gnu11 -mno-red-zone -msse2 -Wno-unused-parameter -fpic \
       -fno-stack-protector -funsigned-char -Wno-pointer-sign \
       -Wno-discarded-qualifiers -g
QEMUFLAGS=-m 512M -monitor stdio -no-reboot
_KOBJ=boot kernel vm memory video
KOBJ=$(shell echo "$(_KOBJ)" | sed 's/\([a-z]\+\)/obj\/\1.o/g')
KINC=$(shell echo kernel/*.h)
_UOBJ=init vga hello
UOBJ=$(shell echo "$(_UOBJ)" | sed 's/\([a-z]\+\)/obj\/\1.xvm/g')

xon.iso: toolchain/wasp/opcodes.py iso/boot/kxon iso/boot/xon.rd
	grub-mkrescue -o $@ iso

iso/boot/kxon: kernel/linker.ld $(KOBJ)
	ld -m elf_x86_64 -o $@ -T kernel/linker.ld $(KOBJ)
iso/boot/xon.rd: $(UOBJ)
	python mkrd.py $^ > $@

obj/%.xvm: userland/%.py
	python toolchain/xpythonc.py < $< > $@
obj/%.xvm: userland/%.wasp
	python toolchain/wasp.py $< $@
toolchain/wasp/opcodes.py: kernel/vm_opcodes.h
	python toolchain/wasp/gen_opcodes.py $< > $@

obj/%.o: kernel/%.asm
	nasm -f elf64 -o $@ $<
obj/%.o: kernel/%.c $(KINC)
	$(CC) $(CFLAGS) -o $@ $<

.PHONY: run clean

run: xon.iso
	qemu-system-x86_64 $(QEMUFLAGS) -cdrom $<
clean:
	rm obj/*
