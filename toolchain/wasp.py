import sys
from wasp.parser import parser
from wasp.codegen import Compiler
from wasp.asm import Assembler

if len(sys.argv) != 3:
    print("Usage: %s <input> <output>" % sys.argv[0])
    sys.exit(1)

def line_stream(s):
    yield from s.split("\n")

with open(sys.argv[1]) as f:
    code = f.read()

ast = parser.parse(code)
c = Compiler()
c.compile(ast)
asm = c.get_asm()
a = Assembler(line_stream(asm))
exe = a.exe()

with open(sys.argv[2], "wb") as f:
    f.write(exe)
