import uuid
import struct
from wasp.parser import parser

def fnv64(b):
    h = 0xCBF29CE484222325
    for c in b:
        h = ((h * 0x100000001B3) & 0xFFFFFFFFFFFFFFFF) ^ c
    return h

def xmap(f, l):
    return list(map(f, l))

binops = {
    "+": "add",
    "-": "sub",
    "*": "mul",
    "/": "div",
    "%": "mod",
    "&": "and",
    "|": "or",
    "^": "xor",
    "<<": "shl",
    ">>": "shr",
    ">>>": "shar",
    "==": "eq",
    "!=": "neq",
    "<": "lt",
    ">": "gt",
    "<=": "le",
    ">=": "ge",
}
unops = {
    "~": "inv",
    "-": "neg",
    "!": "bool_not",
}

def expand_macro(argdef, args, ast):
    if isinstance(ast, list):
        return xmap(lambda x: expand_macro(argdef, args, x), ast)
    elif isinstance(ast, tuple):
        if ast[0] == "id":
            if ast[1] in argdef:
                return args[argdef.index(ast[1])]
        else:
            return (*map(lambda x: expand_macro(argdef, args, x), ast),)
    return ast

class Compiler:
    def __init__(self):
        self.asm = []
        self.asm_pre = []
        self.asm_post = []
        self.globals = []
        self.locals = []
        self.used_vars = []
        self.macros = {}
        self.macros_v = {}
        self.loops = []
        self.compiling_match = False
    def _emit(self, opc, *args):
        b = []
        b.append(ops[opc])
        for x in args:
            if isinstance(x, list):
                b += x
            else:
                b += varint(x)
        return bytes(b)
    def emit(self, opc, *args):
        self.asm.append((opc, *args))
    def compile(self, ast):
        xmap(self.c_stmt, ast)
        self.emit("syscall", 0)
    def c_stmt(self, ast):
        if ast[0] == "expr":
            self.c_expr(ast[1])
        elif ast[0] == "drop":
            self.c_expr(ast[1])
            self.emit("drop")
        elif ast[0] == "assign":
            self.c_expr(ast[2])
            self.c_lvalue(ast[1])
        elif ast[0] == "ret":
            self.emit("leave")
        elif ast[0] == "rete":
            self.c_expr(ast[1])
            self.emit("leave")
        elif ast[0] == "while":
            a = str(uuid.uuid4())
            b = str(uuid.uuid4())
            self.loops.append((a, b))
            self.emit("label", a)
            self.c_expr(ast[1])
            self.emit("jump_false", b)
            xmap(self.c_stmt, ast[2])
            self.emit("jump", a)
            self.emit("label", b)
            self.loops.pop()
        elif ast[0] == "break":
            self.emit("jump", self.loops[-1][1])
        elif ast[0] == "continue":
            self.emit("jump", self.loops[-1][0])
        elif ast[0] == "macro":
            self.macros[ast[1]] = (ast[2], ast[3])
        elif ast[0] == "macroa":
            self.macros_v[ast[1]] = ast[2]
        elif ast[0] == "macroif":
            for cond, body in ast[1]:
                if cond == None or self.c_constexpr(cond):
                    xmap(self.c_stmt, body)
                    break
        elif ast[0] == "include":
            with open(ast[1], "r") as f:
                xmap(self.c_stmt, parser.parse(f.read()))
    def c_expr(self, ast):
        try:
            e = self.c_constexpr(ast)
            if isinstance(e, int):
                self.emit("push_int", e)
            elif isinstance(e, float):
                self.emit("push_float",
                        struct.unpack("<Q", struct.pack("<d", e))[0])
            return
        except SyntaxError:
            pass
        if ast is None:
            self.emit("push_int", 0)
        elif ast[0] == "num":
            if isinstance(ast[1], int):
                self.emit("push_int", ast[1])
            elif isinstance(ast[1], float):
                self.emit("push_float",
                        struct.unpack("<Q", struct.pack("<d", ast[1]))[0])
        elif ast[0] == "id":
            if ast[1] in self.macros_v:
                self.c_expr(self.macros_v[ast[1]])
                return
            if self.compiling_match:
                self.emit("placeholder")
                self.used_vars.append(ast)
                return
            if self.locals and ast[1] in self.locals[-1]:
                self.emit("push_local", self.locals[-1].index(ast[1]))
            elif ast[1] in self.globals:
                self.emit("push_global", self.globals.index(ast[1]))
            else:
                for i, x in enumerate(reversed(self.locals)):
                    if ast[1] in x:
                        self.emit("push_clocal", x.index(ast[1]), i)
                        return
                raise SyntaxError("unknown variable %s" % ast[1])
        elif ast[0] == "pop":
            if self.compiling_match:
                self.emit("placeholder")
                self.used_vars.append(ast)
        elif ast[0] == "list":
            for x in ast[1]:
                self.c_expr(x)
            self.emit("push_int", 0)
            for _ in range(len(ast[1])):
                self.emit("cons")
        elif ast[0] == "tuple":
            for x in ast[1]:
                self.c_expr(x)
            self.emit("tuple", len(ast[1]))
        elif ast[0] == "cons":
            self.c_expr(ast[1])
            self.c_expr(ast[2])
            self.emit("cons")
        elif ast[0] == "str":
            self.emit("bytestring", len(ast[1]), list(ast[1]))
        elif ast[0] == "lstr":
            self.c_expr(("list", xmap(lambda x: ("num", ord(x)), ast[1])))
        elif ast[0] == "sym":
            self.emit("push_int", fnv64(ast[1]))
        elif ast[0] == "binop":
            if ast[1] in ["&&", "||"]:
                s = ["jump_true", "jump_false"][ast[1] == "&&"]
                n = str(uuid.uuid4())
                l = str(uuid.uuid4())
                self.c_expr(ast[2])
                self.emit(s, n)
                self.c_expr(ast[3])
                self.emit(s, n)
                self.emit("push_int", int(ast[1] == "&&"))
                self.emit("jump", l)
                self.emit("label", n)
                self.emit("push_int", int(ast[1] == "||"))
                self.emit("label", l)
            else:
                self.c_expr(ast[2])
                self.c_expr(ast[3])
                self.emit(binops[ast[1]])
        elif ast[0] == "unop":
            self.c_expr(ast[2])
            self.emit(unops[ast[1]])
        elif ast[0] == "if":
            labels = []
            end = str(uuid.uuid4())
            has_else = False
            for cond, _ in ast[1]:
                l = str(uuid.uuid4())
                if cond:
                    self.c_expr(cond)
                    self.emit("jump_true", l)
                else:
                    self.emit("jump", l)
                    has_else = True
                labels.append(l)
            if not has_else:
                self.emit("jump", end)
            for i, x in enumerate(ast[1]):
                _, body = x
                self.emit("label", labels[i])
                xmap(self.c_stmt, body)
                if i != len(ast[1]) - 1:
                    self.emit("jump", end)
            self.emit("label", end)
        elif ast[0] == "match":
            labels = []
            uv = []
            end = str(uuid.uuid4())
            has_else = False
            for match, _ in ast[1]:
                l = str(uuid.uuid4())
                if match:
                    self.c_expr(match[1])
                    self.used_vars = []
                    self.compiling_match = True
                    self.c_expr(match[2])
                    self.compiling_match = False
                    uv.append(self.used_vars)
                    self.emit("match")
                    self.emit("jump_true", l)
                else:
                    self.emit("jump", l)
                    has_else = True
                labels.append(l)
            if not has_else:
                self.emit("jump", end)
            for i, x in enumerate(ast[1]):
                match, body = x
                self.emit("label", labels[i])
                if match and uv[i]:
                    self.emit("unpack")
                    for x in uv[i]:
                        self.c_lvalue(x)
                xmap(self.c_stmt, body)
                if i != len(ast[1]) - 1:
                    self.emit("jump", end)
            self.emit("label", end)
        elif ast[0] == "call":
            if ast[1][0] == "id" and ast[1][1] in self.macros:
                macro = self.macros[ast[1][1]]
                xmap(self.c_stmt, expand_macro(macro[0], ast[2], macro[1]))
                return
            elif ast[1][0] == "id" and ast[1][1] == "b__nasm":
                import os
                asm = str(uuid.uuid4()) + ".asm"
                out = str(uuid.uuid4()) + ".bin"
                with open(asm, "wb") as f:
                    f.write(ast[2][0][1])
                os.system("nasm -f bin -o %s %s" % (out, asm))
                with open(out, "rb") as f:
                    b = f.read()
                os.unlink(asm)
                os.unlink(out)
                self.c_expr(("str", b))
                return
            elif ast[1][0] == "member":
                self.c_expr(("call", ("id", ast[1][2]), [ast[1][1]] + ast[2]))
                return
            xmap(self.c_expr, reversed(ast[2]))
            self.c_expr(ast[1])
            self.emit("call")
        elif ast[0] == "index":
            self.c_expr(ast[1])
            self.c_expr(ast[2])
            self.emit("index")
        elif ast[0] == "slice":
            self.c_expr(ast[1])
            self.c_expr(ast[2][0])
            self.c_expr(ast[2][1])
            self.emit("slice")
        elif ast[0] == "func":
            name = str(uuid.uuid4())
            self.locals.append([])
            t = self.asm
            self.asm = []
            for x in ast[1]:
                self.locals[-1].append(x)
                self.c_lvalue(("local", x))
            if not ast[2] or ast[2][-1][0] not in ["ret", "rete"]:
                ast[2].append(("ret",))
            xmap(self.c_stmt, ast[2])
            self.asm = [("label", name),
                        ("enter", len(self.locals[-1]))] + self.asm
            self.locals.pop()
            self.asm_post += self.asm
            self.asm = t
            self.emit("func", name)
        elif ast[0] == "asm":
            self.asm.append((ast[1].decode("utf8"),))
        elif ast[0] == "asma":
            xmap(self.c_expr, reversed(ast[2]))
            self.asm.append((ast[1].decode("utf8"),))
    def c_lvalue(self, ast):
        if ast[0] in ["local", "id"]:
            if self.locals:
                if ast[1] not in self.locals[-1]:
                    self.locals[-1].append(ast[1])
                self.emit("set_local", self.locals[-1].index(ast[1]))
            else:
                if ast[1] not in self.globals:
                    self.globals.append(ast[1])
                self.emit("set_global", self.globals.index(ast[1]))
        elif ast[0] == "global":
            if ast[1] not in self.globals:
                self.globals.append(ast[1])
            self.emit("set_global", self.globals.index(ast[1]))
        elif ast[0] == "tuple":
            self.emit("unpack")
            xmap(self.c_lvalue, reversed(ast[1]))
        elif ast[0] == "index":
            self.c_expr(ast[1])
            self.c_expr(ast[2])
            self.emit("set_index")
    def c_constexpr(self, ast):
        if ast is None:
            pass
        elif ast[0] == "num":
            return ast[1]
        elif ast[0] == "id" and ast[1] in self.macros_v:
            return self.c_constexpr(self.macros_v[ast[1]])
        elif ast[0] == "sym":
            return fnv64(ast[1])
        elif ast[0] == "binop":
            op = ast[1]
            if op == "&&":
                op = "and"
            elif op == "||":
                op = "or"
            return int(eval("%d %s %d" % (self.c_constexpr(ast[2]),
                op, self.c_constexpr(ast[3]))))
        elif ast[0] == "unop":
            op = ast[1]
            if op == "!":
                op = "not"
            return int(eval("%s %d" % (op, self.c_constexpr(ast[2]))))
        elif ast[0] == "call" and ast[1][0] == "id" and \
                ast[1][1] == "macro_defined":
            return int(ast[2][0][1] in self.macros or \
                    ast[2][0][1] in self.macros_v)
        raise SyntaxError("expression isn't constant")
    def get_asm(self):
        s = ""
        s += "globals %s\n" % " ".join(self.globals)
        for l in self.asm_pre + self.asm + self.asm_post:
            s += " ".join(map(str, map(lambda x: \
                "[%s]" % "".join(map("%02X".__mod__, x)) \
                if isinstance(x, list) else x, l))) + "\n"
        return s

if __name__ == "__main__":
    import parser
    ast = parser.parser.parse(open(0).read())
    c = Compiler()
    try:
        c.compile(ast)
        c.get_asm()
    except SyntaxError as ex:
        print(ex)
