from collections import OrderedDict
from wasp.opcodes import OPCODES
import re
import struct

class Assembler:
    def __init__(self, stream):
        self.stream = stream
        self.bytecode = b""
        self.globals = []
    def fnv64(self, b):
        h = 0xCBF29CE484222325
        for c in b:
            h = ((h * 0x100000001B3) & 0xFFFFFFFFFFFFFFFF) ^ c
        return h
    def varint(self, n, l=-1):
        sign = n < 0
        n = abs(n)
        b = []
        while 1:
            if (n & ~0b111111) == 0 and len(b) >= l - 1:
                b.append(n | (sign << 6))
                break
            else:
                b.append((n & 0x7F) | (1 << 7))
                n >>= 7
        return b
    def emit(self, opc, *args):
        b = []
        b.append(OPCODES[opc])
        for x in args:
            if isinstance(x, list):
                b += x
            else:
                b += self.varint(x)
        return bytes(b)
    def parse_opr(self, o):
        if re.match(r"^0x[0-9A-Fa-f]+$", o):
            return int(o[2:], 16)
        elif re.match(r"^-?\d+$", o):
            return int(o)
        elif o[0] == "[" and o[-1] == "]":
            o = o[1:-1]
            s = []
            for x in zip(o[::2], o[1::2]):
                s.append(int("".join(x), 16))
            return s
        return o
    def parse(self):
        for l_ in self.stream:
            l = l_.strip()
            if not l:
                continue
            if l[0] == ";":
                continue
            l = l.split(" ")
            if l[0] == "globals":
                self.globals += l[1:]
                continue
            yield (l[0], *map(self.parse_opr, l[1:]))
    def exe(self):
        exe = list(b"XVM0" + b"\0" * 20)

        asm = list(self.parse())
        labels = {}
        pc = 0
        for x in asm:
            if x[0] == "label":
                labels[x[1]] = pc
            else:
                pc += 1
                for i in x[1:]:
                    if isinstance(i, str):
                        pc += 4
                    elif isinstance(i, list):
                        pc += len(i)
                    else:
                        pc += len(self.varint(i))
        b = b""
        pc = 0
        for x in asm:
            if x[0] == "label":
                continue
            a = []
            for i in x[1:]:
                if isinstance(i, str):
                    if i[0] == "@":
                        a.append(self.varint(labels[i[1:]], 4))
                    else:
                        a.append(self.varint(labels[i] - pc, 4))
                else:
                    a.append(i)
            p = self.emit(x[0], *a)
            pc += len(p)
            b += p

        chunks = OrderedDict()
        chunks["code"] = b

        b = b""
        for x in self.globals:
            b += struct.pack("<Q", self.fnv64(x.encode("utf8")))
        chunks["glob"] = b

        exe = b""
        for k, v in chunks.items():
            if len(exe) % 4 != 0:
                exe += b"\xAA" * (4 - len(exe) % 4)
            exe += k.encode("utf8")
            exe += struct.pack("<I", len(v))
            exe += v

        return b"\x55xvm" + struct.pack("<I", len(exe)) + exe

if __name__ == "__main__":
    open(1, "wb").write(Assembler(open(0)).exe())
