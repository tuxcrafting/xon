import ply.lex as lex
import re

tokens = (
    "NUMBER", "ID", "STRING", "LSTRING", "SYMBOL",
    "UNDERSCORE",
    "KW_MACRO",
    "KW_VAR", "KW_GLOBAL",
    "KW_INCLUDE", "KW_ASM",
    "KW_IF", "KW_ELSE", "KW_ELSEIF",
    "KW_MATCH", "KW_ELSEMATCH", "KW_IN",
    "KW_WHILE",
    "KW_RETURN", "KW_BREAK", "KW_CONTINUE",
    "PLUS", "MINUS", "TIMES", "DIVIDE", "MODULUS",
    "BITAND", "BITOR", "BITXOR", "BITNOT", "SHL", "SHR", "SHAR",
    "LT", "GT", "LE", "GE", "EQ", "NE",
    "LOGAND", "LOGOR", "LOGNOT",
    "ASSIGN", "ARROW",
    "PERIOD", "COLON", "COMMA", "SEMICOLON", "ELLIPSIS",
    "LPAREN", "RPAREN",
    "LSQUARE", "RSQUARE",
    "LBRACE", "RBRACE",
)

def t_NUMBER(t):
    r"'.|[+-]?(0x[0-9a-fA-F]+|0o[0-7]+|0b[01]+|(\d+(\.\d*)?|\.\d+)(e[-+]?\d+)?)"
    if t.value[0] == "'":
        t.value = str(ord(t.value[1]))
    t.value = eval(t.value)
    return t
def t_ID(t):
    r"_|([a-zA-Z]\w*)"
    if t.value == t.value.lower() and "KW_" + t.value.upper() in tokens:
        t.type = "KW_" + t.value.upper()
    elif t.value == "_":
        t.type = "UNDERSCORE"
    return t

ESCAPES = {
    "n": "\n",
    "t": "\t",
    "r": "\r",
    "f": "\f",
    "v": "\v",
    "e": "\033",
    "\n": "\n",
}

def t_STRING(t):
    r"[:$&]?\"([^\\\"]|\\(.|\n))*\""
    if t.value[0] == ":":
        t.type = "SYMBOL"
        t.value = t.value[1:]
    elif t.value[0] == "$":
        t.type = "LSTRING"
        t.value = t.value[1:]
    elif t.value[0] == "&":
        s = t.value[2:-1]
        l = []
        for x, y in zip(s[::2], s[1::2]):
            l.append(int(x + y, 16))
        t.value = bytes(l)
        return t
    t.value = re.sub(r"\\(.|\n)",
            lambda g: ESCAPES.get(g.group(1), g.group(0)), t.value[1:-1])
    t.value = t.value.encode("utf8")
    return t

t_PLUS = r"\+"
t_MINUS = r"-"
t_TIMES = r"\*"
t_DIVIDE = r"/"
t_MODULUS = r"%"

t_BITAND = r"&"
t_BITOR = r"\|"
t_BITXOR = r"\^"
t_BITNOT = r"~"
t_SHL = r"<<"
t_SHR = r">>"
t_SHAR = r">>>"

t_LT = r"<"
t_GT = r">"
t_LE = r"<="
t_GE = r">="
t_EQ = r"=="
t_NE = r"!="

t_LOGAND = r"&&"
t_LOGOR = r"\|\|"
t_LOGNOT = r"!"

t_ASSIGN = r"="
t_ARROW = r"=>"

t_PERIOD = r"\."
t_COLON = r":"
t_COMMA = r","
t_SEMICOLON = r";"
t_ELLIPSIS = r"\.\.\."

t_LPAREN = r"\("
t_RPAREN = r"\)"

t_LSQUARE = r"\["
t_RSQUARE = r"\]"

t_LBRACE = r"\{"
t_RBRACE = r"\}"

t_ignore = " \t"
t_ignore_COMMENT = r"\#.*"

def t_newline(t):
    r"\n+"
    t.lexer.lineno += len(t.value)

def t_error(t):
    raise SyntaxError("illegal character '%s' at line %d" \
            % (t.value[0], t.lineno))

lexer = lex.lex()

if __name__ == "__main__":
    lexer.input(open(0).read())
    while True:
        try:
            tok = lexer.token()
        except SyntaxError as ex:
            print(ex)
            break
        if not tok:
            break
        print(tok)
