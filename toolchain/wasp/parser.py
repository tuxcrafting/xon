import ply.yacc as yacc
from wasp.lexer import tokens

def make_list(name, elm, delim=None):
    def empty(p):
        p[0] = []
    def head(p):
        p[0] = [p[1]]
    def tail(p):
        if delim:
            p[0] = p[1] + [p[3]]
        else:
            p[0] = p[1] + [p[2]]
    empty.__doc__ = "%s :" % name
    head.__doc__ = "%s : %s" % (name, elm)
    if delim:
        tail.__doc__ = "%s : %s %s %s" % (name, name, delim, elm)
    else:
        tail.__doc__ = "%s : %s %s" % (name, name, elm)
    return empty, head, tail

precedence = (
    ("left", "KW_IF", "KW_ELSEIF", "KW_MATCH", "KW_ELSEMATCH"),
    ("left", "KW_IN"),
    ("left", "COMMA"),
    ("left", "LOGOR"),
    ("left", "LOGAND"),
    ("left", "BITOR"),
    ("left", "BITXOR"),
    ("left", "BITAND"),
    ("nonassoc", "EQ", "NE"),
    ("nonassoc", "LT", "GT", "LE", "GE"),
    ("right", "COLON"),
    ("left", "SHL", "SHR", "SHAR"),
    ("left", "PLUS", "MINUS"),
    ("left", "TIMES", "DIVIDE", "MODULUS"),
    ("right", "UMINUS", "BITNOT", "LOGNOT"),
    ("left", "LPAREN", "LSQUARE", "LBRACE"),
    ("left", "PERIOD"),
)
start = "stmtlist"

p_stmtlist_empty, p_stmtlist_head, p_stmtlist_tail = \
        make_list("stmtlist", "stmt")

def p_stmt_expr(p):
    "stmt : expr SEMICOLON"
    p[0] = ("expr", p[1])
def p_stmt_drop(p):
    "stmt : UNDERSCORE ASSIGN expr SEMICOLON"
    p[0] = ("drop", p[3])
def p_stmt_assign(p):
    "stmt : lvalue ASSIGN expr SEMICOLON"
    p[0] = ("assign", p[1], p[3])
def p_stmt_include(p):
    "stmt : KW_INCLUDE STRING SEMICOLON"
    p[0] = ("include", p[2])
def p_stmt_while(p):
    "stmt : KW_WHILE expr LBRACE stmtlist RBRACE SEMICOLON"
    p[0] = ("while", p[2], p[4])
def p_stmt_break(p):
    "stmt : KW_BREAK SEMICOLON"
    p[0] = ("break",)
def p_stmt_continue(p):
    "stmt : KW_CONTINUE SEMICOLON"
    p[0] = ("continue",)
def p_stmt_return(p):
    "stmt : KW_RETURN SEMICOLON"
    p[0] = ("ret",)
def p_stmt_returnexpr(p):
    "stmt : KW_RETURN expr SEMICOLON"
    p[0] = ("rete", p[2])
def p_stmt_macro(p):
    "stmt : KW_MACRO ID LPAREN argnamelist RPAREN LBRACE stmtlist RBRACE SEMICOLON"
    p[0] = ("macro", p[2], p[4], p[7])
def p_stmt_macroassign(p):
    "stmt : KW_MACRO ID ASSIGN expr SEMICOLON"
    p[0] = ("macroa", p[2], p[4])
def p_stmt_staticconditional(p):
    "stmt : KW_MACRO KW_IF expr LBRACE stmtlist RBRACE else SEMICOLON"
    p[0] = ("macroif", [(p[3], p[5])] + p[7])

def p_expr_number(p):
    "expr : NUMBER"
    p[0] = ("num", p[1])
def p_expr_id(p):
    "expr : ID"
    p[0] = ("id", p[1])
def p_expr_string(p):
    "expr : STRING"
    p[0] = ("str", p[1])
def p_expr_lstring(p):
    "expr : LSTRING"
    p[0] = ("lstr", p[1])
def p_expr_symbol(p):
    "expr : SYMBOL"
    p[0] = ("sym", p[1])
def p_expr_cons(p):
    "expr : expr COLON expr"
    p[0] = ("cons", p[1], p[3])
def p_expr_tuple(p):
    "expr : LBRACE arglist RBRACE"
    p[0] = ("tuple", p[2])
def p_expr_list(p):
    "expr : LSQUARE arglist RSQUARE"
    p[0] = ("list", p[2])
def p_expr_pop(p):
    "expr : UNDERSCORE"
    p[0] = ("pop",)
def p_expr_function(p):
    "expr : LPAREN argnamelist RPAREN ARROW LBRACE stmtlist RBRACE"
    p[0] = ("func", p[2], p[6])
def p_expr_paren(p):
    "expr : LPAREN expr RPAREN"
    p[0] = p[2]
def p_expr_binop(p):
    """expr : expr PLUS expr
            | expr MINUS expr
            | expr TIMES expr
            | expr DIVIDE expr
            | expr MODULUS expr
            | expr SHL expr
            | expr SHR expr
            | expr SHAR expr
            | expr LT expr
            | expr GT expr
            | expr LE expr
            | expr GE expr
            | expr EQ expr
            | expr NE expr
            | expr BITAND expr
            | expr BITOR expr
            | expr BITXOR expr
            | expr LOGAND expr
            | expr LOGOR expr"""
    p[0] = ("binop", p[2], p[1], p[3])
def p_expr_unop(p):
    """expr : MINUS expr %prec UMINUS
            | BITNOT expr
            | LOGNOT expr"""
    p[0] = ("unop", p[1], p[2])
def p_expr_funcall(p):
    "expr : expr LPAREN arglist RPAREN"
    p[0] = ("call", p[1], p[3])
def p_expr_if(p):
    "expr : KW_IF expr LBRACE stmtlist RBRACE else"
    p[0] = ("if", [(p[2], p[4])] + p[6])
def p_expr_match(p):
    "expr : KW_MATCH matchin LBRACE stmtlist RBRACE elsematch"
    p[0] = ("match", [(p[2], p[4])] + p[6])
def p_expr_asm(p):
    "expr : KW_ASM STRING"
    p[0] = ("asm", p[2])
def p_expr_asma(p):
    "expr : KW_ASM STRING LPAREN arglist RPAREN"
    p[0] = ("asma", p[2], p[4])
def p_expr_slice(p):
    "expr : expr LSQUARE slice RSQUARE"
    p[0] = ("slice", p[1], p[3])
def p_expr_index(p):
    "expr : expr LSQUARE expr RSQUARE"
    p[0] = ("index", p[1], p[3])
def p_expr_period(p):
    "expr : expr PERIOD ID"
    p[0] = ("member", p[1], p[3])

def p_slice(p):
    "slice : expr ELLIPSIS expr"
    p[0] = (p[1], p[3])

def p_matchpattern_cons(p):
    "matchpattern : matchpattern COLON matchpattern"
    p[0] = ("cons", p[1], p[3])
def p_matchpattern_tuple(p):
    "matchpattern : LBRACE patternlist RBRACE"
    p[0] = ("tuple", p[2])
def p_matchpattern_lval(p):
    "matchpattern : ASSIGN lvalue"
    p[0] = ("lval", p[2])
def p_matchpattern_expr(p):
    "matchpattern : expr"
    p[0] = p[1]

def p_else_empty(p):
    r"else :"
    p[0] = []
def p_else_else(p):
    "else : KW_ELSE LBRACE stmtlist RBRACE"
    p[0] = [(None, p[3])]
def p_else_elseif(p):
    "else : KW_ELSEIF expr LBRACE stmtlist RBRACE else"
    p[0] = [(p[2], p[4])] + p[6]

def p_elsematch_empty(p):
    "elsematch :"
    p[0] = []
def p_elsematch_else(p):
    "elsematch : KW_ELSE LBRACE stmtlist RBRACE"
    p[0] = [(None, p[3])]
def p_elsematch_elseif(p):
    "elsematch : KW_ELSEMATCH matchin LBRACE stmtlist RBRACE elsematch"
    p[0] = [(p[2], p[4])] + p[6]

def p_matchin(p):
    "matchin : expr KW_IN matchpattern"
    p[0] = ("in", p[1], p[3])

def p_lvalue_tuple(p):
    "lvalue : LBRACE lvaluelist RBRACE"
    p[0] = ("tuple", p[2])
def p_lvalue_1(p):
    "lvalue : lvalue1"
    p[0] = p[1]

def p_lvalue1_id(p):
    "lvalue1 : ID"
    p[0] = ("id", p[1])
def p_lvalue1_var(p):
    """lvalue1 : KW_VAR ID
               | KW_GLOBAL ID"""
    p[0] = ("local" if p[1] == "var" else "global", p[2])
def p_lvalue1_index(p):
    "lvalue1 : expr LSQUARE expr RSQUARE"
    p[0] = ("index", p[1], p[3])

p_arglist_empty, p_arglist_head, p_arglist_tail = \
        make_list("arglist", "expr", "COMMA")
p_patternlist_empty, p_patternlist_head, p_patternlist_tail = \
        make_list("patternlist", "matchpattern", "COMMA")
p_argnamelist_empty, p_argnamelist_head, p_argnamelist_tail = \
        make_list("argnamelist", "ID", "COMMA")
p_lvaluelist_empty, p_lvaluelist_head, p_lvaluelist_tail = \
        make_list("lvaluelist", "lvalue", "COMMA")

def p_error(p):
    raise SyntaxError("syntax error: %s" % p)

parser = yacc.yacc()

if __name__ == "__main__":
    try:
        print(parser.parse(open(0).read()))
    except SyntaxError as ex:
        print(ex)
