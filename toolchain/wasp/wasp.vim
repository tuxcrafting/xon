" Vim syntax file for WASP
" Language: WASP
" Maintainer: tuxcrafting

if exists("b:current_syntax")
    finish
endif

syn match waspComment '#[^\n]*'

syn keyword waspKeyword if elseif match elsematch in else while return break continue var global macro include asm

syn match waspId '[a-zA-Z]\w*'

syn match waspNumber '[+-]\?\d\+\(\.\d*\)\?\(e[-+]\?\d\+\)\?'
syn match waspNumber '[+-]\?\.\d\+\(e[-+]\?\d\+\)\?'
syn match waspNumber '[+-]\?0x[0-9a-fA-F]\+'
syn match waspNumber '[+-]\?0o[0-7]\+'
syn match waspNumber '[+-]\?0b[01]\+'

syn match waspStringEsc contained '\\.'
syn region waspString start='"' end='"' contains=waspStringEsc
syn region waspString start='l"' end='"' contains=waspStringEsc
syn region waspString start=':"' end='"' contains=waspStringEsc
syn region waspString start='&"' end='"'

syn match waspChar '\'.'

hi def link waspComment Comment
hi def link waspKeyword Statement
hi def link waspNumber Constant
hi def link waspStringEsc Special
hi def link waspString Constant
hi def link waspChar Constant
