from ast import *
from collections import OrderedDict
import re
import struct
import sys
import uuid

def fnv64(b):
    h = 0xCBF29CE484222325
    for c in b:
        h = ((h * 0x100000001B3) & 0xFFFFFFFFFFFFFFFF) ^ c
    return h

def xmap(f, l):
    return list(map(f, l))

ops = {}
for x in open("kernel/vm_opcodes.h").read().split("\n"):
    m = re.search(r"#define OP_(\w+) 0x([0-9A-F]{2})", x)
    if m:
        ops[m.group(1).lower()] = int(m.group(2), 16)
r_ops = dict(list(map(lambda x: (x[1], x[0]), ops.items())))

binops = {
    Add: "add",
    Sub: "sub",
    Mult: "mul",
    Div: "div",
    Mod: "mod",
    BitAnd: "and",
    BitOr: "or",
    BitXor: "xor",
    MatMult: "cons",
    LShift: "shl",
    RShift: "shr",
}
unaryops = {
    Invert: "inv",
    USub: "neg",
    Not: "bool_not",
}
compares = {
    Eq: "eq",
    NotEq: "neq",
    Lt: "lt",
    Gt: "gt",
    LtE: "le",
    GtE: "ge",
}
nameconstants = {
    None: ("push_int", 0),
    False: ("push_int", 0),
    True: ("push_int", 1),
}

def varint(n, l=-1):
    sign = n < 0
    n = abs(n)
    b = []
    while 1:
        if (n & ~0b111111) == 0 and len(b) >= l - 1:
            b.append(n | (sign << 6))
            break
        else:
            b.append((n & 0x7F) | (1 << 7))
            n >>= 7
    return b

def parse_varint(b):
    l = []
    n = 0
    i = 0
    while 1:
        c = next(b)
        l.append(c)
        if c & 0x80:
            n += (c & 0x7F) << i
            i += 7
        else:
            n += (c & 0x3F) << i
            if c & 0x40:
                n = -n
            break
    return l, n

def rpad(n, s):
    return s + " " * (n - len(s))

def disasm(b):
    b = iter(b)
    pc = 0
    try:
        while 1:
            o = next(b)
            op = r_ops[o]
            x = None
            xl = []
            y = None
            yl = []
            s = None
            sl = []
            if op.startswith("push_") or op.startswith("set_") \
            or op.startswith("jump") or op in ["syscall", "enter", "swap",
                                               "bytestring", "func", "tuple"]:
                xl, x = parse_varint(b)
            if op == "push_clocal":
                yl, y = parse_varint(b)
            if op == "bytestring":
                s = b""
                for _ in range(x):
                    a = next(b)
                    s += bytes([a])
                    sl.append(a)
            ol = [o] + xl + yl + sl
            print(
                "%07X %s %s%s%s%s" % (
                pc,
                rpad(22, "".join(map("%02X".__mod__, ol))),
                op,
                " 0x%X" % x if xl else "",
                ", 0x%X" % y if yl else "",
                ", %s" % repr(s)[1:] if s is not None else ""),
                file=sys.stderr)
            pc += len(ol)
    except StopIteration:
        pass

class Compiler:
    def __init__(self):
        self.bytecode = b""
        self.bytecode_pre = b""
        self.bytecode_post = b""
        self.asm = []
        self.asm_pre = []
        self.asm_post = []
        self.globals = []
        self.locals = []
        self.used_vars = []
        self.macros = {}
        self.loops = []
        self.compiling_match = False
    def _emit(self, opc, *args):
        b = []
        b.append(ops[opc])
        for x in args:
            if isinstance(x, list):
                b += x
            else:
                b += varint(x)
        return bytes(b)
    def emit(self, opc, *args):
        self.asm.append((opc, *args))
    def compile(self, ast):
        #print(dump(ast), file=sys.stderr)
        xmap(self.c_stmt, ast.body)
        self.emit("syscall", 0)
    def c_stmt(self, ast):
        if isinstance(ast, Expr):
            self.c_expr(ast.value)
            self.emit("drop")
        elif isinstance(ast, Assign):
            self.c_expr(ast.value)
            for _ in range(len(ast.targets) - 1):
                self.emit("dup")
            for t in ast.targets:
                if isinstance(t, Tuple):
                    self.emit("unpack")
                    for x in reversed(t.elts):
                        self.c_lvalue(x)
                else:
                    self.c_lvalue(t)
        elif isinstance(ast, AugAssign):
            self.c_stmt(Assign(targets=[ast.target],
                value=BinOp(left=ast.target, op=ast.op, right=ast.value)))
        elif isinstance(ast, FunctionDef):
            if ast.name.startswith("M_"):
                c = compile(Module(body=[ast]), "", "exec")
                exec(c)
                self.macros[ast.name[2:]] = eval(ast.name)
                return
            t = self.asm
            self.asm = self.asm_pre
            self.emit("func", "f_%s" % ast.name)
            self.c_lvalue(Name(id=ast.name, ctx=Store()))
            self.locals.append([])
            self.asm_pre = self.asm
            self.asm = []
            for x in ast.args.args:
                self.locals[-1].append(x.arg)
                self.c_lvalue(Name(id=x.arg, ctx=Store()))
            if not ast.body or not isinstance(ast.body[-1], Return):
                ast.body.append(Return(value=None))
            xmap(self.c_stmt, ast.body)
            self.asm = [("label", "f_%s" % ast.name),
                        ("enter", len(self.locals[-1]))] + self.asm
            self.locals.pop()
            self.asm_post += self.asm
            self.asm = t
        elif isinstance(ast, Return):
            self.c_expr(ast.value)
            self.emit("leave")
        elif isinstance(ast, If):
            n = str(uuid.uuid4())
            e = str(uuid.uuid4())
            self.c_expr(ast.test)
            self.emit("jump_false", n)
            xmap(self.c_stmt, ast.body)
            if ast.orelse:
                self.emit("jump", e)
            self.emit("label", n)
            xmap(self.c_stmt, ast.orelse)
            self.emit("label", e)
        elif isinstance(ast, While):
            a = str(uuid.uuid4())
            b = str(uuid.uuid4())
            self.loops.append((a, b))
            self.emit("label", a)
            self.c_expr(ast.test)
            self.emit("jump_false", b)
            xmap(self.c_stmt, ast.body)
            self.emit("jump", a)
            self.emit("label", b)
            self.loops.pop()
        elif isinstance(ast, Break):
            self.emit("jump", self.loops[-1][1])
        elif isinstance(ast, Continue):
            self.emit("jump", self.loops[-1][0])
    def c_expr(self, ast):
        if ast is None:
            self.emit("push_int", 0)
        elif isinstance(ast, Num):
            if isinstance(ast.n, int):
                self.emit("push_int", ast.n)
            elif isinstance(ast.n, float):
                self.emit("push_float",
                        struct.unpack("<Q", struct.pack("<d", ast.n))[0])
        elif isinstance(ast, Name):
            if self.compiling_match:
                self.emit("placeholder")
                self.used_vars.append(ast)
                return
            if ast.id in self.globals:
                self.emit("push_global", self.globals.index(ast.id))
            elif self.locals and ast.id in self.locals[-1]:
                self.emit("push_local", self.locals[-1].index(ast.id))
            else:
                for i, x in enumerate(reversed(self.locals)):
                    if ast.id in x:
                        self.emit("push_clocal", x.index(ast.id), i)
                        return
                raise SyntaxError("can't reference variable %s" % ast.id)
        elif isinstance(ast, NameConstant):
            self.emit(*nameconstants[ast.value])
        elif isinstance(ast, List):
            for x in ast.elts:
                self.c_expr(x)
            self.emit("push_int", 0)
            for _ in range(len(ast.elts)):
                self.emit("cons")
        elif isinstance(ast, Tuple):
            for x in ast.elts:
                self.c_expr(x)
            self.emit("tuple", len(ast.elts))
        elif isinstance(ast, Str):
            self.emit("bytestring", len(ast.s), list(ast.s.encode("utf8")))
        elif isinstance(ast, Bytes):
            self.emit("push_int", fnv64(ast.s))
        elif isinstance(ast, BinOp):
            self.c_expr(ast.left)
            self.c_expr(ast.right)
            for k, v in binops.items():
                if isinstance(ast.op, k):
                    self.emit(v)
                    break
        elif isinstance(ast, UnaryOp):
            self.c_expr(ast.operand)
            for k, v in unaryops.items():
                if isinstance(ast.op, k):
                    self.emit(v)
                    break
        elif isinstance(ast, BoolOp):
            s = ["jump_true", "jump_false"][isinstance(ast.op, And)]
            n = str(uuid.uuid4())
            l = str(uuid.uuid4())
            for x in ast.values:
                self.c_expr(x)
                self.emit(s, n)
            self.emit("push_int", int(isinstance(ast.op, And)))
            self.emit("jump", l)
            self.emit("label", n)
            self.emit("push_int", int(isinstance(ast.op, Or)))
            self.emit("label", l)
        elif isinstance(ast, Compare):
            if isinstance(ast.ops[0], In):
                l = str(uuid.uuid4())
                self.c_expr(ast.left)
                saved_uv = self.used_vars
                self.used_vars = []
                self.compiling_match = True
                self.c_expr(ast.comparators[0])
                self.compiling_match = False
                self.emit("match")
                self.emit("dup")
                self.emit("jump_false", l)
                self.emit("swap", 2)
                self.emit("unpack")
                for x in reversed(self.used_vars):
                    self.c_lvalue(x)
                self.used_vars = saved_uv
                self.emit("label", l)
                return
            self.c_expr(ast.left)
            self.c_expr(ast.comparators[0])
            for k, v in compares.items():
                if isinstance(ast.ops[0], k):
                    self.emit(v)
                    break
        elif isinstance(ast, Call):
            if isinstance(ast.func, Name):
                if ast.func.id == "__asm":
                    xmap(self.c_expr, reversed(ast.args[1:]))
                    s = literal_eval(ast.args[0])
                    if isinstance(s, str):
                        self.emit(s)
                    elif isinstance(s, tuple):
                        self.emit(*s)
                    elif isinstance(s, list):
                        for x in s:
                            self.emit(*x)
                    return
                elif ast.func.id in self.macros:
                    ast = self.macros[ast.func.id](*ast.args)
                    self.c_expr(ast)
                    return
            xmap(self.c_expr, reversed(ast.args))
            self.c_expr(ast.func)
            self.emit("call")
        elif isinstance(ast, Subscript):
            self.c_expr(ast.value)
            if isinstance(ast.slice, Index):
                self.c_expr(ast.slice.value)
                self.emit("index")
            elif isinstance(ast.slice, Slice):
                self.c_expr(ast.slice.lower)
                self.c_expr(ast.slice.upper)
                self.emit("slice")
        elif isinstance(ast, IfExp):
            n = str(uuid.uuid4())
            e = str(uuid.uuid4())
            self.c_expr(ast.test)
            self.emit("jump_false", n)
            self.c_expr(ast.body)
            self.emit("jump", e)
            self.emit("label", n)
            self.c_expr(ast.orelse)
            self.emit("label", e)
        elif isinstance(ast, Lambda):
            self.locals.append([])
            n = str(uuid.uuid4())
            t = self.asm
            self.asm = []
            for x in ast.args.args:
                self.locals[-1].append(x.arg)
                self.c_lvalue(Name(id=x.arg, ctx=Store()))
            self.c_expr(ast.body)
            self.emit("leave")
            self.asm = [("label", n),
                        ("enter", len(self.locals[-1]))] + self.asm
            self.asm_post += self.asm
            self.asm = t
            self.locals.pop()
            self.emit("func", n)
    def c_lvalue(self, ast):
        if isinstance(ast, Name):
            if self.locals and ast.id in self.locals[-1]:
                self.emit("set_local", self.locals[-1].index(ast.id))
                return
            elif ast.id in self.globals:
                self.emit("set_global", self.globals.index(ast.id))
                return
            if self.locals:
                self.locals[-1].append(ast.id)
                self.emit("set_local", self.locals[-1].index(ast.id))
            else:
                self.globals.append(ast.id)
                self.emit("set_global", self.globals.index(ast.id))
        elif isinstance(ast, Subscript):
            self.c_expr(ast.value)
            if isinstance(ast.slice, Index):
                self.c_expr(ast.slice.value)
                self.emit("set_index")
    def exe(self):
        exe = list(b"XVM0" + b"\0" * 20)

        asm = self.asm_pre + self.asm + self.asm_post
        labels = {}
        pc = 0
        for x in asm:
            if x[0] == "label":
                labels[x[1]] = pc
            else:
                pc += 1
                for i in x[1:]:
                    if isinstance(i, str):
                        pc += 4
                    elif isinstance(i, list):
                        pc += len(i)
                    else:
                        pc += len(varint(i))
        b = b""
        pc = 0
        for x in asm:
            if x[0] == "label":
                continue
            a = []
            for i in x[1:]:
                if isinstance(i, str):
                    if i[0] == "@":
                        a.append(varint(labels[i[1:]], 4))
                    else:
                        a.append(varint(labels[i] - pc, 4))
                else:
                    a.append(i)
            p = self._emit(x[0], *a)
            pc += len(p)
            b += p
        #disasm(b)

        chunks = OrderedDict()
        chunks["code"] = b

        b = b""
        for x in self.globals:
            b += struct.pack("<Q", fnv64(x.encode("utf8")))
        chunks["glob"] = b

        exe = b""
        for k, v in chunks.items():
            if len(exe) % 4 != 0:
                exe += b"\xAA" * (4 - len(exe) % 4)
            exe += k.encode("utf8")
            exe += struct.pack("<I", len(v))
            exe += v

        return b"\x55xvm" + struct.pack("<I", len(exe)) + exe

c = Compiler()
c.compile(parse(open(0).read()))
open(1, "wb").write(c.exe())
